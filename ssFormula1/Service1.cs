﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Timers;
using System.Xml;
using Rebex.Net;
using Xceed.Zip;
using Xceed.FileSystem;
using System.Globalization;
using log4net;

namespace ssFormula1
{
    public partial class Service1 : ServiceBase
    {
        public System.Timers.Timer sysTimer;
        public string dbConnString = ConfigurationManager.AppSettings["sql"];
        public SqlConnection dbConn;
        bool dbConnected = true;
        runInfo curRun;

        private ILog _logger = null;

        public Service1()
        {
            InitializeComponent();

            _logger = LogManager.GetLogger(typeof(Service1));
        }

        internal void TestStartupAndStop(string[] args)
        {
            Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A";
            run();
            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += timerElapsed;
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
            //Console.ReadLine();
            Thread.Sleep(600000);
        }

        protected override void OnStart(string[] args)
        {
            Xceed.Zip.Licenser.LicenseKey = "ZIN23-X0UZT-5AHMP-4A2A";

            checkFolders();

            sysTimer = new System.Timers.Timer(5000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        protected override void OnStop()
        {

        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            Thread t = new Thread(run);
            t.IsBackground = true;
            t.Start();
        }

        private void run()
        {
            curRun = new runInfo();
            curRun.Start = DateTime.Now;

            _logger.Info("Processing Started.");

            try
            {
                dbConn = new SqlConnection(dbConnString);
                dbConn.Open();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in run()", ex);

                dbConnected = false;
            }

            if (dbConnected && dbConn != null)
            {
                try
                {
                    dbConn.StateChange += new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in run()", ex);

                    curRun.Errors.Add(ex.Message.ToString());
                }

                getFiles();
                getLiveFiles();
                processFiles(false);
                archiveFiles();

                try
                {
                    dbConn.StateChange -= new System.Data.StateChangeEventHandler(dbConnChanged);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in run()", ex);

                    curRun.Errors.Add(ex.Message.ToString());
                }
            }
            else
            {
                _logger.Error("Failed to connect to the database");

                curRun.Errors.Add("Failed to connect to the database");
            }

            curRun.End = DateTime.Now;
            endRun();
            writeInfo(curRun.End.ToString("s"));

            if (dbConn != null)
            {
                dbConn.Dispose();
            }

            sysTimer = new System.Timers.Timer(60000);
            sysTimer.Elapsed += new ElapsedEventHandler(timerElapsed);
            sysTimer.AutoReset = false;
            sysTimer.Enabled = true;
        }

        private void endRun()
        {
            try
            {
                string errors = "";

                foreach (string tmpString in curRun.Errors)
                {
                    errors += "Error: " + tmpString + Environment.NewLine;
                }

                SqlCommand SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_F1Runs (startTime, endTime, downloads, downloadErrors, errors, timeStamp) Values (@startTime, @endTime, @downloads, @downloadErrors, @errors, @timeStamp)", dbConn);
                SqlQuery.Parameters.Add("@startTime", SqlDbType.DateTime).Value = curRun.Start;
                SqlQuery.Parameters.Add("@endTime", SqlDbType.DateTime).Value = curRun.End;
                SqlQuery.Parameters.Add("@downloads", SqlDbType.Int).Value = curRun.Downloads;
                SqlQuery.Parameters.Add("@downloadErrors", SqlDbType.Int).Value = curRun.DownloadErrors;
                SqlQuery.Parameters.Add("@errors", SqlDbType.VarChar).Value = errors;
                SqlQuery.Parameters.Add("@timeStamp", SqlDbType.VarChar).Value = DateTime.Now;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in endRun()", ex);

                curRun.Errors.Add("Updating run details - " + ex.Message);
            }
        }

        private void dbConnChanged(object sender, EventArgs e)
        {
            if (dbConnected && dbConn != null)
            {
                if (dbConn.State == ConnectionState.Broken || dbConn.State == ConnectionState.Closed)
                {
                    try
                    {
                        dbConn = new SqlConnection(dbConnString);
                        dbConn.Open();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occurred in dbConnChanged()", ex);

                        dbConnected = false;
                    }
                }
            }
        }

        private void writeInfo(string Date)
        {
            try
            {
                string tmpDate = Date.Replace(':', '_').Replace('/', '_');
                FileStream Fs = new FileStream("c:/SuperSport/Motorsport/LastRun/lastRun_" + tmpDate + ".txt", FileMode.Create, FileAccess.Write);
                StreamWriter Sw = new StreamWriter(Fs);
                Sw.BaseStream.Seek(0, SeekOrigin.Begin);
                Sw.WriteLine("Start: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("End: " + curRun.Start.ToString("yyyy-MM-dd HH:mm:ss"));
                Sw.WriteLine("Downloads: " + curRun.Downloads.ToString());
                Sw.WriteLine("Download Errors: " + curRun.DownloadErrors.ToString());
                foreach (string tmpString in curRun.Errors)
                {
                    Sw.WriteLine("Error: " + tmpString);
                }
                Sw.Close();
                Fs.Dispose();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in writeInfo()", ex);
            }
        }

        private void getFiles()
        {
            try
            {
                _logger.Info("Retrieving Files for processing.");
                FileInfo fi;

                if (Directory.Exists("C:/inetpub/ftproot/pauser/motorsport"))
                {
                    foreach (string file in Directory.GetFiles("C:/inetpub/ftproot/pauser/motorsport"))
                    {
                        fi = new FileInfo(file);
                        if (fi.Name.IndexOf("xml") >= 0 && fi.Length > 0)
                        {
                            fi.CopyTo("c:/SuperSport/Motorsport/Xml/Temp/" + fi.Name);
                            fi.Delete();
                        }
                    }
                }

                if (Directory.Exists("C:/inetpub/ftproot/teamtalk/motorsport"))
                {
                    foreach (string file in Directory.GetFiles("C:/inetpub/ftproot/teamtalk/motorsport"))
                    {
                        fi = new FileInfo(file);
                        if (fi.Name.IndexOf("xml") >= 0 && fi.Length > 0)
                        {
                            fi.CopyTo("c:/SuperSport/Motorsport/Xml/Temp/" + fi.Name);
                            fi.Delete();
                        }
                    }
                }

                if (Directory.Exists("C:/inetpub/ftproot/teamtalk/motorsport/formula1"))
                {
                    foreach (string file in Directory.GetFiles("C:/inetpub/ftproot/teamtalk/motorsport/formula1"))
                    {
                        fi = new FileInfo(file);
                        if (fi.Name.IndexOf("xml") >= 0 && fi.Length > 0)
                        {
                            fi.CopyTo("c:/SuperSport/Motorsport/Xml/Temp/" + fi.Name);
                            fi.Delete();
                        }
                    }
                }

                //if (Directory.Exists("C:/inetpub/ftproot/teamtalk/motorsport/superbikes"))
                //{
                //    foreach (string file in Directory.GetFiles("C:/inetpub/ftproot/teamtalk/motorsport/superbikes"))
                //    {
                //        fi = new FileInfo(file);
                //        if (fi.Name.IndexOf("xml") >= 0 && fi.Length > 0)
                //        {
                //            fi.CopyTo("c:/SuperSport/Motorsport/Xml/Temp/" + fi.Name);
                //            fi.Delete();
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in getFiles()", ex);

                curRun.Errors.Add(ex.Message.ToString());
            }

            //Rebex.Net.Ftp curFtp = new Rebex.Net.Ftp();
            //string ftpServer = "152.111.116.3";
            //string ftpUser = "PASport";
            //string ftpPassword = "Supersp0rt";
            //string ftpDirectory = "formula1";

            //try
            //{
            //    curFtp.Connect(ftpServer);
            //    curFtp.Login(ftpUser, ftpPassword);
            //    curFtp.ChangeDirectory(ftpDirectory);
            //    Rebex.Net.FtpList tmpList = new Rebex.Net.FtpList();
            //    tmpList = curFtp.GetList();
            //    int Total = tmpList.Count;
            //    if (Total > 0)
            //    {
            //        int Downloaded = 0;
            //        int fileErrors = 0;
            //        for (int i = 0; i <= Total - 1; i++)
            //        {
            //            Rebex.Net.FtpItem curItem = tmpList[i];
            //            if (curItem.Size > 0)
            //            {
            //                try
            //                {
            //                    curFtp.GetFile(curItem.Name, "c:/SuperSport/Motorsport/Xml/Temp/" + curItem.Name);
            //                    curFtp.KeepAlive();
            //                    Downloaded += 1;
            //                    curFtp.DeleteFile(curItem.Name);
            //                }
            //                catch (Exception ex1)
            //                {
            //                    fileErrors += 1;
            //                    curRun.Errors.Add(curItem.Name + " - " + ex1.Message);
            //                }
            //            }
            //        }
            //        curRun.Downloads = Downloaded;
            //        curRun.DownloadErrors = fileErrors;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    curRun.Errors.Add(ex.Message.ToString());
            //}
            //finally
            //{
            //    if (curFtp != null)
            //    {
            //        curFtp.Dispose();
            //    }
            //}
        }

        private void getLiveFiles()
        {
            //Rebex.Net.Ftp curFtp = new Rebex.Net.Ftp();
            //string ftpServer = "152.111.116.3";
            //string ftpUser = "PASport";
            //string ftpPassword = "Supersp0rt";
            //string ftpDirectory = "motorsport/formula1";

            //try
            //{
            //    curFtp.Connect(ftpServer);
            //    curFtp.Login(ftpUser, ftpPassword);
            //    curFtp.ChangeDirectory(ftpDirectory);
            //    Rebex.Net.FtpList tmpList = new Rebex.Net.FtpList();
            //    tmpList = curFtp.GetList();
            //    int Total = tmpList.Count;
            //    if (Total > 0)
            //    {
            //        int Downloaded = 0;
            //        int fileErrors = 0;
            //        for (int i = 0; i <= Total - 1; i++)
            //        {
            //            Rebex.Net.FtpItem curItem = tmpList[i];
            //            if (curItem.Size > 0)
            //            {
            //                try
            //                {
            //                    curFtp.GetFile(curItem.Name, "c:/SuperSport/Motorsport/Xml/Temp/" + curItem.Name);
            //                    curFtp.KeepAlive();
            //                    Downloaded += 1;
            //                    curFtp.DeleteFile(curItem.Name);
            //                }
            //                catch (Exception ex1)
            //                {
            //                    fileErrors += 1;
            //                    curRun.Errors.Add(curItem.Name + " - " + ex1.Message);
            //                }
            //            }
            //        }
            //        curRun.Downloads = Downloaded;
            //        curRun.DownloadErrors = fileErrors;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    curRun.Errors.Add(ex.Message.ToString());
            //}
            //finally
            //{
            //    if (curFtp != null)
            //    {
            //        curFtp.Dispose();
            //    }
            //}
        }

        private void processFiles(bool test)
        {
            if (test)
            {
                //dbConn = new SqlConnection("Server=10.16.208.128,55540;UID=tlns;PWD=pr0v1dence;DATABASE=SuperSportZone");
                dbConn = new SqlConnection("Server=197.80.203.40;UID=supersms;PWD=5m5RrR4;DATABASE=SuperSportZone");
                dbConn.Open();
            }

            _logger.Info("Processing of Files started.");
            
            foreach (string tmpFile in Directory.GetFiles("c:/SuperSport/Motorsport/Xml/Temp"))
            {
                try
                {
                    FileInfo Fi = new FileInfo(tmpFile);
                    bool Done = false;

                    if (Fi.Name.ToLower().IndexOf(".xml") >= 0 && Fi.Name.ToLower().IndexOf("weatherupdate") < 0 && Fi.Length > 0)
                    {
                        try
                        {
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load("c:/SuperSport/Motorsport/Xml/Temp/" + Fi.Name.ToLower());

                            if (Fi.Name.ToLower().IndexOf("livemotorsport") >= 0)
                            {
                                Done = liveLeaderboard(xmlDoc, Fi.Name.ToLower());
                            }
                            else if (Fi.Name.ToLower().IndexOf("520") >= 0 || Fi.Name.ToLower().IndexOf("510") >= 0)
                            {
                                if (Fi.Name.ToLower().StartsWith("schedules"))
                                {
                                    Done = Schedule(xmlDoc, Fi.Name.ToLower());
                                }
                                if (Fi.Name.ToLower().StartsWith("raceresults"))
                                {
                                    Done = RaceResults(xmlDoc, Fi.Name.ToLower());
                                }
                                if (Fi.Name.ToLower().StartsWith("tables"))
                                {
                                    Done = Standings(xmlDoc, Fi.Name.ToLower());
                                }
                            }
                            else if (Fi.Name.ToLower().IndexOf("motorsportcommentary") >= 0)
                            {
                                Done = Commentary(xmlDoc, Fi.Name.ToLower());
                            }
                            else if (Fi.Name.ToLower().IndexOf("weatherupdate") >= 0)
                            {
                                Done = true;
                            }
                            else
                            {
                                foreach (XmlNode F1LiveNode in xmlDoc.SelectNodes("FormulaOneLive"))
                                {
                                    int eventId = -1;
                                    int roundId = -1;
                                    string roundName = "";
                                    int stageId = -1;
                                    string stageName = "";
                                    foreach (XmlAttribute F1LiveAttribute in F1LiveNode.Attributes)
                                    {
                                        switch (F1LiveAttribute.Name)
                                        {
                                            case "eventId":
                                                eventId = Convert.ToInt32(F1LiveAttribute.Value.ToString().Replace("C", ""));
                                                break;
                                            case "roundId":
                                                roundId = Convert.ToInt32(F1LiveAttribute.Value.ToString().Replace("C", ""));
                                                break;
                                            case "roundName":
                                                roundName = F1LiveAttribute.Value.ToString();
                                                break;
                                            case "stageId":
                                                stageId = Convert.ToInt32(F1LiveAttribute.Value.ToString().Replace("C", ""));
                                                break;
                                            case "stageName":
                                                stageName = F1LiveAttribute.Value.ToString();
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    foreach (XmlNode F1LiveSubNode in F1LiveNode.ChildNodes)
                                    {

                                        switch (F1LiveSubNode.Name)
                                        {
                                            case "StageDetails":
                                                Done = stageDetails(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "Drivers":
                                                Done = Drivers(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "TrackConditions":

                                                break;
                                            case "Weather":
                                                Done = weather(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "Status":
                                                Done = status(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "DriverDetails":
                                                Done = driverDetails(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "StartingGrid":
                                                Done = startingGrid(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "LeaderBoard":
                                                Done = Leaderboards(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "SectorAnalysis":

                                                break;
                                            case "Results":
                                                Done = Results(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "FastestLap":
                                                Done = fastestLap(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                                break;
                                            case "DriverTable":
                                                Done = driverTable(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            case "TeamTable":
                                                Done = teamTable(F1LiveSubNode, eventId, roundId, roundName, stageId, stageName);
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }

                            xmlDoc = null;
                        }
                        catch (Exception ex1)
                        {
                            _logger.Error("Error occurred in processFiles()", ex1);
                            Done = false;
                        }
                    }
                    else
                    {
                        Done = true;
                    }

                    if (Done)
                    {
                        if (!(Fi.Name.ToLower().IndexOf(".dtd") >= 0))
                        {
                            Fi.CopyTo("c:/SuperSport/Motorsport/Xml/Processed/" + Fi.Name, true);
                        }
                    }
                    else
                    {
                        if (!(Fi.Name.ToLower().IndexOf(".dtd") >= 0))
                        {
                            Fi.CopyTo("c:/SuperSport/Motorsport/Xml/Unprocessed/" + Fi.Name, true);
                        }
                    }
                    if (!(Fi.Name.ToLower().IndexOf(".dtd") >= 0))
                    {
                        Fi.Delete();
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in processFiles()", ex);

                    curRun.Errors.Add(ex.Message.ToString());
                }
            }

            _logger.Info("Processing of Files ended.");

            if (test)
            {
                dbConn.Close();
            }
        }

        private bool liveLeaderboard(XmlDocument xmlDoc, string fileName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                foreach (XmlNode xmlNode in xmlDoc.SelectNodes("XML"))
                {
                    Session session = new Session();

                    int fileYear = 1900;
                    int fileMonth = 1;
                    int fileDay = 1;
                    int fileHours = 0;
                    int fileMinutes = 0;
                    int fileSeconds = 0;

                    try
                    {
                        string fileDate = fileName.Split('_')[3];
                        fileYear = Convert.ToInt32(fileDate.Substring(0, 4));
                        fileMonth = Convert.ToInt32(fileDate.Substring(4, 2));
                        fileDay = Convert.ToInt32(fileDate.Substring(6, 2));
                        fileHours = Convert.ToInt32(fileDate.Substring(8, 2));
                        fileMinutes = Convert.ToInt32(fileDate.Substring(10, 2));
                        fileSeconds = Convert.ToInt32(fileDate.Substring(12, 2));

                        session.FileTime = new DateTime(fileYear, fileMonth, fileDay, fileHours, fileMinutes, fileSeconds);
                    }
                    catch(Exception ex)
                    {
                        _logger.Error("Error occurred in liveLeaderboard()", ex);
                        session.FileTime = new DateTime(1900, 1, 1, 0, 0, 0);
                    }

                    foreach (XmlNode LiveNode in xmlNode.SelectNodes("TITLE"))
                    {
                        foreach (XmlAttribute SubAttribute in LiveNode.Attributes)
                        {
                            switch (SubAttribute.Name)
                            {
                                case "LABEL":
                                    session.RaceName = SubAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    foreach (XmlNode LiveNode in xmlNode.SelectNodes("EVENTSUMMARY"))
                    {
                        foreach (XmlAttribute SubAttribute in LiveNode.Attributes)
                        {
                            switch (SubAttribute.Name)
                            {
                                case "SPORTID":
                                    session.RaceSport = Convert.ToInt32(SubAttribute.Value.ToString());
                                    break;
                                case "RACEID":
                                    session.RaceId = Convert.ToInt32(SubAttribute.Value.ToString());
                                    break;
                                case "CPID":
                                    session.RaceCompetition = Convert.ToInt32(SubAttribute.Value.ToString());
                                    break;
                                case "SESSIONID":
                                    session.Id = Convert.ToInt32(SubAttribute.Value.ToString());
                                    break;
                                case "FXDATE":
                                    if (!string.IsNullOrEmpty(SubAttribute.Value.ToString()))
                                    {
                                        session.Date = Convert.ToDateTime(SubAttribute.Value.ToString());
                                    }
                                    break;
                                case "FXTIME":
                                    if (!string.IsNullOrEmpty(SubAttribute.Value.ToString()))
                                    {
                                        session.Time = new DateTime(1900, 1, 1, Convert.ToInt32(SubAttribute.Value.ToString().Split(':')[0]), Convert.ToInt32(SubAttribute.Value.ToString().Split(':')[1]), 0);
                                    }
                                    break;
                                default:
                                    break;
                            }

                            if (session.Date != null && session.Time != null)
                            {
                                session.Date = new DateTime(session.Date.Year, session.Date.Month, session.Date.Day, session.Time.Hour, session.Time.Minute, 0);
                            }
                            else if (session.Date == null)
                            {
                                session.Date = new DateTime(1900, 1, 1, 0, 0, 0);
                            }
                        }

                        foreach (XmlNode SessionNode in LiveNode.SelectNodes("SESSION"))
                        {
                            foreach (XmlAttribute SubAttribute in SessionNode.Attributes)
                            {
                                switch (SubAttribute.Name)
                                {
                                    case "NAME":
                                        session.Name = SubAttribute.Value.ToString();
                                        break;
                                    case "LAPS":
                                        if (!string.IsNullOrEmpty(SubAttribute.Value.ToString()))
                                        {
                                            session.Laps = Convert.ToInt32(SubAttribute.Value.ToString());
                                        }
                                        else
                                        {
                                            session.Laps = 0;
                                        }
                                        break;
                                    case "STATUS":
                                        session.Status = SubAttribute.Value.ToString();
                                        break;
                                    case "TYPE":
                                        session.Type = SubAttribute.Value.ToString();
                                        break;
                                    case "NO":
                                        session.Number = Convert.ToInt32(SubAttribute.Value.ToString());
                                        break;
                                    case "COUNTDOWNMINS":
                                        session.Minutes = SubAttribute.Value.ToString();
                                        if (String.IsNullOrEmpty(session.Minutes))
                                        {
                                            session.Minutes = "0";
                                        }
                                        break;
                                    case "COMMENTS":
                                        session.Comments = SubAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                    
                    foreach (XmlNode LiveNode in xmlNode.SelectNodes("EVENTDETAIL"))
                    {
                        foreach (XmlAttribute SubAttribute in LiveNode.Attributes)
                        {
                            switch (SubAttribute.Name)
                            {
                                case "VENUENAME":
                                    session.RaceVenue = SubAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    int sqlId = -1;
                    SqlQuery = new SqlCommand("SELECT Id FROM Tennis.dbo.sky_live_sessions WHERE (SessionId = @sessionId) AND (sessionFileTime = @sessionFileTime)", dbConn);
                    SqlQuery.Parameters.Add("@sessionId", SqlDbType.Int).Value = session.Id;
                    SqlQuery.Parameters.Add("@sessionFileTime", SqlDbType.DateTime).Value = session.FileTime;
                    try
                    {
                        sqlId = Convert.ToInt32(SqlQuery.ExecuteScalar());
                    }
                    catch
                    {
                        sqlId = -1;
                    }

                    if (sqlId > 0)
                    {
                        SqlQuery = new SqlCommand("UPDATE Tennis.dbo.sky_live_sessions Set raceId = @raceId, raceSport = @raceSport, raceName = @raceName, raceVenue = @raceVenue, sessionId = @sessionId, sessionType = @sessionType, sessionName = @sessionName, sessionNumber = @sessionNumber, sessionDate = @sessionDate, sessionLaps = @sessionLaps, sessionMinutes = @sessionMinutes, sessionStatus = @sessionStatus, sessionComments = @sessionComments, sessionCreated = @sessionCreated, sessionFileTime = @sessionFileTime, raceCompetition = @raceCompetition WHERE (Id = @id)", dbConn);
                        SqlQuery.Parameters.Add("@id", SqlDbType.Int).Value = sqlId;
                        SqlQuery.Parameters.Add("@raceId", SqlDbType.Int).Value = session.RaceId;
                        SqlQuery.Parameters.Add("@raceSport", SqlDbType.Int).Value = session.RaceSport;
                        SqlQuery.Parameters.Add("@raceName", SqlDbType.VarChar).Value = session.RaceName;
                        SqlQuery.Parameters.Add("@raceVenue", SqlDbType.VarChar).Value = session.RaceVenue;
                        SqlQuery.Parameters.Add("@sessionId", SqlDbType.Int).Value = session.Id;
                        SqlQuery.Parameters.Add("@sessionType", SqlDbType.VarChar).Value = session.Type;
                        SqlQuery.Parameters.Add("@sessionName", SqlDbType.VarChar).Value = session.Name;
                        SqlQuery.Parameters.Add("@sessionNumber", SqlDbType.Int).Value = session.Number;
                        SqlQuery.Parameters.Add("@sessionDate", SqlDbType.DateTime).Value = session.Date;
                        SqlQuery.Parameters.Add("@sessionLaps", SqlDbType.Int).Value = session.Laps;
                        SqlQuery.Parameters.Add("@sessionMinutes", SqlDbType.VarChar).Value = session.Minutes;
                        SqlQuery.Parameters.Add("@sessionStatus", SqlDbType.VarChar).Value = session.Status;
                        SqlQuery.Parameters.Add("@sessionComments", SqlDbType.VarChar).Value = session.Comments;
                        SqlQuery.Parameters.Add("@sessionCreated", SqlDbType.DateTime).Value = DateTime.Now;
                        SqlQuery.Parameters.Add("@sessionFileTime", SqlDbType.DateTime).Value = session.FileTime;
                        SqlQuery.Parameters.Add("@raceCompetition", SqlDbType.Int).Value = session.RaceCompetition;
                        SqlQuery.ExecuteNonQuery();
                    }
                    else
                    {
                        SqlQuery = new SqlCommand("DELETE FROM Tennis.dbo.sky_live_sessions WHERE (SessionId = @sessionId) AND (sessionFileTime = @sessionFileTime); INSERT INTO Tennis.dbo.sky_live_sessions (raceId, raceSport, raceName, raceVenue, sessionId, sessionType, SessionName, sessionNumber, sessionDate, sessionLaps, sessionMinutes, sessionStatus, sessionComments, sessionCreated, sessionFileTime, raceCompetition) VALUES (@raceId, @raceSport, @raceName, @raceVenue, @sessionId, @sessionType, @sessionName, @sessionNumber, @sessionDate, @sessionLaps, @sessionMinutes, @sessionStatus, @sessionComments, @sessionCreated, @sessionFileTime, @raceCompetition); Select SCOPE_IDENTITY();", dbConn);
                        SqlQuery.Parameters.Add("@raceId", SqlDbType.Int).Value = session.RaceId;
                        SqlQuery.Parameters.Add("@raceSport", SqlDbType.Int).Value = session.RaceSport;
                        SqlQuery.Parameters.Add("@raceName", SqlDbType.VarChar).Value = session.RaceName;
                        SqlQuery.Parameters.Add("@raceVenue", SqlDbType.VarChar).Value = session.RaceVenue;
                        SqlQuery.Parameters.Add("@sessionId", SqlDbType.Int).Value = session.Id;
                        SqlQuery.Parameters.Add("@sessionType", SqlDbType.VarChar).Value = session.Type;
                        SqlQuery.Parameters.Add("@sessionName", SqlDbType.VarChar).Value = session.Name;
                        SqlQuery.Parameters.Add("@sessionNumber", SqlDbType.Int).Value = session.Number;
                        SqlQuery.Parameters.Add("@sessionDate", SqlDbType.DateTime).Value = session.Date;
                        SqlQuery.Parameters.Add("@sessionLaps", SqlDbType.Int).Value = session.Laps;
                        SqlQuery.Parameters.Add("@sessionMinutes", SqlDbType.VarChar).Value = session.Minutes;
                        SqlQuery.Parameters.Add("@sessionStatus", SqlDbType.VarChar).Value = session.Status;
                        SqlQuery.Parameters.Add("@sessionComments", SqlDbType.VarChar).Value = session.Comments;
                        SqlQuery.Parameters.Add("@sessionCreated", SqlDbType.DateTime).Value = DateTime.Now;
                        SqlQuery.Parameters.Add("@sessionFileTime", SqlDbType.DateTime).Value = session.FileTime;
                        SqlQuery.Parameters.Add("@raceCompetition", SqlDbType.Int).Value = session.RaceCompetition;
                        sqlId = Convert.ToInt32(SqlQuery.ExecuteScalar());
                    }

                    string leaderboardQuery = "DELETE FROM tennis.dbo.sky_live_leaderboard WHERE (session = " + sqlId + ");";
                    int ordering = 1;

                    foreach (XmlNode LeaderboardNode in xmlNode.SelectNodes("LEADERBOARD/PLAYER"))
                    {
                        Leaderboard leaderboard = new Leaderboard();

                        foreach (XmlAttribute SubAttribute in LeaderboardNode.Attributes)
                        {
                            switch (SubAttribute.Name)
                            {
                                case "POS":
                                    leaderboard.PositionText = SubAttribute.Value.ToString();
                                    break;
                                case "PLNAME":
                                    leaderboard.DriverName = SubAttribute.Value.ToString();
                                    break;
                                case "PLID":
                                    leaderboard.DriverId = Convert.ToInt32(SubAttribute.Value.ToString());
                                    break;
                                case "CTRY":
                                    leaderboard.DriverCountry = SubAttribute.Value.ToString();
                                    break;
                                case "TEAM":
                                    leaderboard.TeamName = SubAttribute.Value.ToString();
                                    break;
                                case "TEAMID":
                                    leaderboard.TeamId = Convert.ToInt32(SubAttribute.Value.ToString());
                                    break;
                                case "GAP":
                                    leaderboard.Gap = SubAttribute.Value.ToString();
                                    break;
                                case "LAPS":
                                    if (!string.IsNullOrEmpty(SubAttribute.Value.ToString()))
                                    {
                                        leaderboard.Laps = Convert.ToInt32(SubAttribute.Value.ToString());
                                    }
                                    break;
                                case "TIME":
                                    leaderboard.Time = SubAttribute.Value.ToString();
                                    break;
                                case "PITS":
                                    if (!string.IsNullOrEmpty(SubAttribute.Value.ToString()))
                                    {
                                        leaderboard.Pits = Convert.ToInt32(SubAttribute.Value.ToString());
                                    }
                                    break;
                                case "COMMENTS":
                                    leaderboard.Comment = SubAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        leaderboard.Position = ordering;
                        ordering += 1;

                        leaderboardQuery += "INSERT INTO tennis.dbo.sky_live_leaderboard (session, position , positionText , driverId , driverName , driverCountry , teamId , teamName , gap , laps , time , pits , comment ) VALUES (" + sqlId + ", " + leaderboard.Position + " , '" + leaderboard.PositionText.Replace("'", "''") + "' , " + leaderboard.DriverId + " , '" + leaderboard.DriverName.Replace("'", "''") + "' , '" + leaderboard.DriverCountry.Replace("'", "''") + "' , " + leaderboard.TeamId + " , '" + leaderboard.TeamName.Replace("'", "''") + "' , '" + leaderboard.Gap.Replace("'", "''") + "' , " + leaderboard.Laps + " , '" + leaderboard.Time.Replace("'", "''") + "' , " + leaderboard.Pits + " , '" + leaderboard.Comment.Replace("'", "''") + "');";
                    }

                    SqlQuery = new SqlCommand(leaderboardQuery, dbConn);
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in liveLeaderboard()", ex);
                curRun.Errors.Add("Live leaderboard - " + fileName + " - " + ex.Message);
            }

            return Processed;
        }

        private bool Schedule(XmlDocument xmlDoc, string fileName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            DateTime fileTime = DateTime.Now;
            int fileYear = 1900;
            int fileMonth = 1;
            int fileDay = 1;
            int fileHours = 0;
            int fileMinutes = 0;
            int fileSeconds = 0;

            try
            {
                fileName = fileName.Substring(fileName.LastIndexOf("_") + 1);
                string fileDate = fileName.Split('.')[0];
                fileYear = Convert.ToInt32(fileDate.Substring(0, 4));
                fileMonth = Convert.ToInt32(fileDate.Substring(4, 2));
                fileDay = Convert.ToInt32(fileDate.Substring(6, 2));
                fileHours = Convert.ToInt32(fileDate.Substring(8, 2));
                fileMinutes = Convert.ToInt32(fileDate.Substring(10, 2));
                fileSeconds = Convert.ToInt32(fileDate.Substring(12, 2));

                fileTime = new DateTime(fileYear, fileMonth, fileDay, fileHours, fileMinutes, fileSeconds);
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in Schedule()", ex);
                fileTime = DateTime.Now;
            }

            try
            {
                foreach (XmlNode xmlNode in xmlDoc.SelectNodes("XML"))
                {
                    foreach (XmlNode xmlDayNode in xmlNode.SelectNodes("DAY"))
                    {
                        DateTime date = new DateTime(1900, 1, 1);

                        foreach (XmlAttribute attribute in xmlDayNode.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "DATE":
                                    date = DateTime.ParseExact(attribute.InnerText, "dd/MM/yyyy", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode xmlCompNode in xmlDayNode.SelectNodes("SPORT/COMPETITION"))
                        {
                            string competition = string.Empty;

                            foreach (XmlAttribute attribute in xmlCompNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "NAME":
                                        competition = attribute.InnerText;
                                        break;
                                    default:
                                        break;
                                }
                            }

                            foreach (XmlNode xmlRaceNode in xmlCompNode.SelectNodes("RACE"))
                            {
                                string race = string.Empty;
                                int raceId = -1;

                                foreach (XmlAttribute attribute in xmlRaceNode.Attributes)
                                {
                                    switch (attribute.Name)
                                    {
                                        case "NAME":
                                            race = attribute.InnerText;
                                            break;
                                        case "ID":
                                            raceId = Convert.ToInt32(attribute.InnerText);
                                            break;
                                        default:
                                            break;
                                    }
                                }

                                foreach (XmlNode xmlSessionNode in xmlRaceNode.SelectNodes("SESSION"))
                                {
                                    string sessionName = string.Empty;
                                    int sessionId = 0;
                                    string sessionVenue = string.Empty;
                                    DateTime sessionDate = date;

                                    foreach (XmlAttribute attribute in xmlSessionNode.Attributes)
                                    {
                                        switch (attribute.Name)
                                        {
                                            case "NAME":
                                                sessionName = attribute.InnerText;
                                                break;
                                            case "ID":
                                                sessionId = Convert.ToInt32(attribute.InnerText);
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    foreach (XmlNode node in xmlSessionNode.ChildNodes)
                                    {
                                        switch (node.Name)
                                        {
                                            case "VENUE":
                                                sessionVenue = node.InnerText;
                                                break;
                                            case "STARTTIME":
                                                if (!String.IsNullOrEmpty(node.InnerText) && node.InnerText.IndexOf(":") >= 0)
                                                {
                                                    string[] tmpArray = node.InnerText.Split(':');
                                                    sessionDate = new DateTime(sessionDate.Year, sessionDate.Month, sessionDate.Day, Convert.ToInt32(tmpArray[0]), Convert.ToInt32(tmpArray[1]), 0);
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }

                                    SqlQuery = new SqlCommand("SELECT COUNT(id) FROM tennis.dbo.motorsport_sessions WHERE (competition = @competition AND year = @year AND raceId = @raceId AND sessionId = @sessionId)", dbConn);
                                    SqlQuery.Parameters.Add("@competition", SqlDbType.VarChar).Value = competition;
                                    SqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = sessionDate.Year;
                                    SqlQuery.Parameters.Add("@raceId", SqlDbType.Int).Value = raceId;
                                    SqlQuery.Parameters.Add("@sessionId", SqlDbType.Int).Value = sessionId;
                                    int present = Convert.ToInt32(SqlQuery.ExecuteScalar());

                                    if (present > 0)
                                    {
                                        SqlQuery = new SqlCommand("UPDATE tennis.dbo.motorsport_sessions SET raceVenue = @raceVenue, sessionName = @sessionName, sessionDate = @sessionDate, modified = GetDate(), fileTime = @fileTime WHERE (competition = @competition AND year = @year AND raceId = @raceId AND sessionId = @sessionId)", dbConn);
                                    }
                                    else
                                    {
                                        SqlQuery = new SqlCommand("INSERT INTO tennis.dbo.motorsport_sessions (competition, year, raceName, raceId, raceVenue, sessionId, sessionName, sessionDate, modified, fileTime) VALUES (@competition, @year, @raceName, @raceId, @raceVenue, @sessionId, @sessionName, @sessionDate, GetDate(), @fileTime)", dbConn);
                                    }
                                    SqlQuery.Parameters.Add("@competition", SqlDbType.VarChar).Value = competition;
                                    SqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = sessionDate.Year;
                                    SqlQuery.Parameters.Add("@raceName", SqlDbType.VarChar).Value = race;
                                    SqlQuery.Parameters.Add("@raceId", SqlDbType.Int).Value = raceId;
                                    SqlQuery.Parameters.Add("@raceVenue", SqlDbType.VarChar).Value = sessionVenue;
                                    SqlQuery.Parameters.Add("@sessionId", SqlDbType.Int).Value = sessionId;
                                    SqlQuery.Parameters.Add("@sessionName", SqlDbType.VarChar).Value = sessionName;
                                    SqlQuery.Parameters.Add("@sessionDate", SqlDbType.DateTime).Value = sessionDate;
                                    SqlQuery.Parameters.Add("@fileTime", SqlDbType.DateTime).Value = fileTime;
                                    SqlQuery.ExecuteNonQuery();
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in Schedule()", ex);
                curRun.Errors.Add("Schedule - " + fileName + " - " + ex.Message);
            }

            return Processed;
        }

        private bool RaceResults(XmlDocument xmlDoc, string fileName)
        {
            bool processed = true;
            SqlCommand SqlQuery;

            DateTime fileTime = DateTime.Now;
            int fileYear = 1900;
            int fileMonth = 1;
            int fileDay = 1;
            int fileHours = 0;
            int fileMinutes = 0;
            int fileSeconds = 0;

            try
            {
                fileName = fileName.Substring(fileName.LastIndexOf("_") + 1);
                string fileDate = fileName.Split('.')[0];
                fileYear = Convert.ToInt32(fileDate.Substring(0, 4));
                fileMonth = Convert.ToInt32(fileDate.Substring(4, 2));
                fileDay = Convert.ToInt32(fileDate.Substring(6, 2));
                fileHours = Convert.ToInt32(fileDate.Substring(8, 2));
                fileMinutes = Convert.ToInt32(fileDate.Substring(10, 2));
                fileSeconds = Convert.ToInt32(fileDate.Substring(12, 2));

                fileTime = new DateTime(fileYear, fileMonth, fileDay, fileHours, fileMinutes, fileSeconds);
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in RaceResults()", ex);
                fileTime = DateTime.Now;
            }

            try
            {
                foreach (XmlNode xmlNode in xmlDoc.SelectNodes("XML"))
                {
                    int sessionId = 0;
                    int raceId = 0;
                    string competition = string.Empty;

                    foreach (XmlNode xmlEventSNode in xmlNode.SelectNodes("EVENTSUMMARY"))
                    {
                        DateTime sessiondDate = new DateTime(1900, 1, 1);
                        string sessionName = string.Empty;
                        int sessionNumber = 1;
                        int sessionLaps = 0;
                        string sessionStatus = string.Empty;
                        string sessionMinutes = "";
                        string sessionComments = string.Empty;

                        foreach (XmlAttribute attribute in xmlEventSNode.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "CPID":
                                    competition = attribute.InnerText.ToString();
                                    break;
                                case "SESSIONID":
                                    bool converted1 = Int32.TryParse(attribute.InnerText, out sessionId);
                                    break;
                                case "RACEID":
                                    bool converted2 = Int32.TryParse(attribute.InnerText, out raceId);
                                    break;
                                case "FXDATE":
                                    try
                                    {
                                        DateTime tempDate = DateTime.ParseExact(attribute.InnerText, "yyyy-MM-dd", CultureInfo.CurrentCulture, DateTimeStyles.None);
                                        sessiondDate = new DateTime(tempDate.Year, tempDate.Month, tempDate.Day, sessiondDate.Hour, sessiondDate.Minute, 0);
                                    }
                                    catch (Exception ex1)
                                    {
                                        sessiondDate = new DateTime(1900, 1, 1);
                                    }
                                    break;
                                case "FXTIME":
                                    try
                                    {
                                        DateTime tempDate = DateTime.ParseExact(attribute.InnerText, "HH:mm", CultureInfo.CurrentCulture, DateTimeStyles.None);
                                        sessiondDate = new DateTime(sessiondDate.Year, sessiondDate.Month, sessiondDate.Day, tempDate.Hour, tempDate.Minute, 0);
                                    }
                                    catch (Exception ex1)
                                    {
                                        sessiondDate = new DateTime(1900, 1, 1);
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode xmlSessionNode in xmlEventSNode.SelectNodes("SESSION"))
                        {
                            foreach (XmlAttribute attribute in xmlSessionNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "NAME":
                                        sessionName = attribute.InnerText;
                                        break;
                                    case "NO":
                                        bool converted1 = Int32.TryParse(attribute.InnerText, out sessionNumber);
                                        break;
                                    case "LAPS":
                                        bool converted2 = Int32.TryParse(attribute.InnerText, out sessionLaps);
                                        break;
                                    case "STATUS":
                                        sessionStatus = attribute.InnerText;
                                        break;
                                    case "COUNTDOWNMINS":
                                        sessionMinutes = attribute.InnerText;
                                        break;
                                    case "COMMENTS":
                                        sessionComments = attribute.InnerText;
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        switch (competition)
                        {
                            case "501":
                                competition = "Formula1";
                                break;
                            case "510":
                                competition = "MotoGP";
                                break;
                            case "520":
                                competition = "SuperBikes";
                                break;
                            default:
                                break;
                        }

                        SqlQuery = new SqlCommand("UPDATE tennis.dbo.motorsport_sessions SET sessionName = @sessionName, sessionNumber = @sessionNumber, sessionDate = @sessionDate, sessionLaps = @sessionLaps, sessionStatus = @sessionStatus, sessionMinutes = @sessionMinutes, sessionComments = @sessionComments, modified = GetDate(), fileTime = @fileTime WHERE (sessionId = @sessionId AND raceId = @raceId AND year = @year AND Competition = @competition)", dbConn);
                        SqlQuery.Parameters.Add("@competition", SqlDbType.VarChar).Value = competition;
                        SqlQuery.Parameters.Add("@sessionId", SqlDbType.Int).Value = sessionId;
                        SqlQuery.Parameters.Add("@raceId", SqlDbType.Int).Value = raceId;
                        SqlQuery.Parameters.Add("@year", SqlDbType.Int).Value = sessiondDate.Year;
                        SqlQuery.Parameters.Add("@sessionName", SqlDbType.VarChar).Value = sessionName;
                        SqlQuery.Parameters.Add("@sessionNumber", SqlDbType.Int).Value = sessionNumber;
                        SqlQuery.Parameters.Add("@sessionDate", SqlDbType.DateTime).Value = sessiondDate;
                        SqlQuery.Parameters.Add("@sessionLaps", SqlDbType.Int).Value = sessionLaps;
                        SqlQuery.Parameters.Add("@sessionStatus", SqlDbType.VarChar).Value = sessionStatus;
                        SqlQuery.Parameters.Add("@sessionMinutes", SqlDbType.VarChar).Value = sessionMinutes;
                        SqlQuery.Parameters.Add("@sessionComments", SqlDbType.VarChar).Value = sessionComments;
                        SqlQuery.Parameters.Add("@fileTime", SqlDbType.DateTime).Value = fileTime;
                        SqlQuery.ExecuteNonQuery();
                    }

                    if (sessionId > 0)
                    {
                        int ordering = 1;
                        StringBuilder sb = new StringBuilder();
                        sb.Append("DELETE FROM tennis.dbo.motorsport_results WHERE (competition = '" + competition + "' AND session = " + sessionId + " AND race = " + raceId + " AND year = " + DateTime.Now.Year + ");");

                        foreach (XmlNode xmlPlayerNode in xmlNode.SelectNodes("LEADERBOARD/PLAYER"))
                        {
                            int position = 0;
                            string positionText = string.Empty;
                            int driverId = 0;
                            string driverName = string.Empty;
                            string driverCountry = string.Empty;
                            int teamId = 0;
                            string teamName = string.Empty;
                            string gap = string.Empty;
                            int laps = 0;
                            string time = string.Empty;
                            int pits = 0;
                            string comment = string.Empty;

                            foreach (XmlAttribute SubAttribute in xmlPlayerNode.Attributes)
                            {
                                switch (SubAttribute.Name)
                                {
                                    case "POS":
                                        positionText = SubAttribute.Value.ToString();
                                        break;
                                    case "PLNAME":
                                        driverName = SubAttribute.Value.ToString();
                                        break;
                                    case "PLID":
                                        bool converted1 = Int32.TryParse(SubAttribute.Value.ToString(), out driverId);
                                        break;
                                    case "CTRY":
                                        driverCountry = SubAttribute.Value.ToString();
                                        break;
                                    case "TEAM":
                                        teamName = SubAttribute.Value.ToString();
                                        break;
                                    case "TEAMID":
                                        bool converted2 = Int32.TryParse(SubAttribute.Value.ToString(), out teamId);
                                        break;
                                    case "GAP":
                                        gap = SubAttribute.Value.ToString();
                                        break;
                                    case "LAPS":
                                        if (!string.IsNullOrEmpty(SubAttribute.Value.ToString()))
                                        {
                                            bool converted3 = Int32.TryParse(SubAttribute.Value.ToString(), out laps);
                                        }
                                        break;
                                    case "TIME":
                                        time = SubAttribute.Value.ToString();
                                        break;
                                    case "PITS":
                                        if (!string.IsNullOrEmpty(SubAttribute.Value.ToString()))
                                        {
                                            bool converted4 = Int32.TryParse(SubAttribute.Value.ToString(), out pits);
                                        }
                                        break;
                                    case "COMMENTS":
                                        comment = SubAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            position = ordering;
                            ordering += 1;

                            sb.Append("INSERT INTO tennis.dbo.motorsport_results (competition, session, race, year, position , positionText , driverId , driverName , driverCountry , teamId , teamName , gap , laps , time , pits , comment ) VALUES ('" + competition + "', " + sessionId + ", " + raceId + ", " + DateTime.Now.Year + ", " + position + " , '" + positionText.Replace("'", "''") + "' , " + driverId + " , '" + driverName.Replace("'", "''") + "' , '" + driverCountry.Replace("'", "''") + "' , " + teamId + " , '" + teamName.Replace("'", "''") + "' , '" + gap.Replace("'", "''") + "' , " + laps + " , '" + time.Replace("'", "''") + "' , " + pits + " , '" + comment.Replace("'", "''") + "');");
                        }

                        if (!String.IsNullOrEmpty(sb.ToString()))
                        {
                            SqlQuery = new SqlCommand(sb.ToString(), dbConn);
                            SqlQuery.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                processed = false;
                _logger.Error("Error occurred in RaceResults()", ex);
                curRun.Errors.Add("Results - " + fileName + " - " + ex.Message);
            }

            return processed;
        }

        private bool Standings(XmlDocument xmlDoc, string fileName)
        {
            bool processed = true;
            SqlCommand SqlQuery;

            DateTime fileTime = DateTime.Now;
            int fileYear = 1900;
            int fileMonth = 1;
            int fileDay = 1;
            int fileHours = 0;
            int fileMinutes = 0;
            int fileSeconds = 0;

            try
            {
                fileName = fileName.Substring(fileName.LastIndexOf("_") + 1);
                string fileDate = fileName.Split('.')[0];
                fileYear = Convert.ToInt32(fileDate.Substring(0, 4));
                fileMonth = Convert.ToInt32(fileDate.Substring(4, 2));
                fileDay = Convert.ToInt32(fileDate.Substring(6, 2));
                fileHours = Convert.ToInt32(fileDate.Substring(8, 2));
                fileMinutes = Convert.ToInt32(fileDate.Substring(10, 2));
                fileSeconds = Convert.ToInt32(fileDate.Substring(12, 2));

                fileTime = new DateTime(fileYear, fileMonth, fileDay, fileHours, fileMinutes, fileSeconds);
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in Standings()", ex);
                fileTime = DateTime.Now;
            }

            try
            {
                foreach (XmlNode xmlNode in xmlDoc.SelectNodes("XML"))
                {
                    string type = string.Empty;
                    string competition = string.Empty;
                    StringBuilder sb = new StringBuilder();
                    
                    foreach (XmlNode xmlTitleNode in xmlNode.SelectNodes("TITLE"))
                    {
                        foreach (XmlAttribute attribute in xmlTitleNode.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "LABEL":
                                    string tmpValue = attribute.InnerText.ToLower();
                                    if (tmpValue.IndexOf("superbike") >= 0)
                                    {
                                        competition = "SuperBikes";
                                    }
                                    else if (tmpValue.IndexOf("formula") >= 0 || tmpValue.IndexOf("f1") >= 0)
                                    {
                                        competition = "Formula1";
                                    }
                                    else if (tmpValue.IndexOf("motogp") >= 0)
                                    {
                                        competition = "MotoGP";
                                    }

                                    if (tmpValue.IndexOf("drivers") >= 0)
                                    {
                                        type = "Drivers";
                                    }
                                    else if (tmpValue.IndexOf("manufacturer") >= 0)
                                    {
                                        type = "Teams";
                                    }
                                    else if (tmpValue.IndexOf("constructor") >= 0)
                                    {
                                        type = "Teams";
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    sb.Append("DELETE FROM tennis.dbo.motorsport_standings WHERE (Competition = '" + competition + "' AND Year = " + DateTime.Now.Year + " AND Type = '" + type + "');");

                    foreach (XmlNode xmlRowNode in xmlNode.SelectNodes("ROW"))
                    {
                        int position = 0;
                        string name = string.Empty;
                        string team = string.Empty;
                        int points = 0;

                        foreach (XmlAttribute attribute in xmlRowNode.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "POSITION":
                                    bool converted = Int32.TryParse(attribute.InnerText, out position);
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode node in xmlRowNode.ChildNodes)
                        {
                            switch (node.Name)
                            {
                                case "NAME":
                                    name = node.InnerText;
                                    break;
                                case "TEAM":
                                    team = node.InnerText;
                                    break;
                                case "POINTS":
                                    bool converted = Int32.TryParse(node.InnerText, out points);
                                    break;
                                default:
                                    break;
                            }
                        }

                        sb.Append("INSERT INTO tennis.dbo.motorsport_standings (competition, year, type, position, positionText, move, name, team, points, timeStamp, created) VALUES ('" + competition + "', " + DateTime.Now.Year + ", '" + type + "', " + position + ", '" + position + "', 0, '" + name.Replace("'", "''") + "', '" + team.Replace("'", "''") + "', " + points + ", '" + fileTime.ToString("yyyy-MM-dd HH:dd:00.000") + "', GetDate());");
                    }

                    if (!String.IsNullOrEmpty(sb.ToString()))
                    {
                        SqlQuery = new SqlCommand(sb.ToString(), dbConn);
                        SqlQuery.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                processed = false;
                _logger.Error("Error occurred in Standings()", ex);
                curRun.Errors.Add("Schedule - " + fileName + " - " + ex.Message);
            }

            return processed;
        }

        private bool Commentary(XmlDocument xmlDoc, string fileName)
        {
            bool processed = true;
            SqlCommand SqlQuery;

            DateTime fileTime = DateTime.Now;
            int fileYear = 1900;
            int fileMonth = 1;
            int fileDay = 1;
            int fileHours = 0;
            int fileMinutes = 0;
            int fileSeconds = 0;

            try
            {
                fileName = fileName.Substring(fileName.LastIndexOf("_") + 1);
                string fileDate = fileName.Split('.')[0];
                fileYear = Convert.ToInt32(fileDate.Substring(0, 4));
                fileMonth = Convert.ToInt32(fileDate.Substring(4, 2));
                fileDay = Convert.ToInt32(fileDate.Substring(6, 2));
                fileHours = Convert.ToInt32(fileDate.Substring(8, 2));
                fileMinutes = Convert.ToInt32(fileDate.Substring(10, 2));
                fileSeconds = Convert.ToInt32(fileDate.Substring(12, 2));

                fileTime = new DateTime(fileYear, fileMonth, fileDay, fileHours, fileMinutes, fileSeconds);
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred in Commentary()", ex);
                fileTime = DateTime.Now;
            }

            try
            {
                foreach (XmlNode xmlNode in xmlDoc.SelectNodes("xml"))
                {
                    foreach (XmlNode commentaryNode in xmlNode.SelectNodes("Commentary"))
                    {
                        int raceId = 0;
                        int sessionId = 0;
                        string competition = "Formula 1";
                        DateTime timeStamp = DateTime.Now;

                        foreach (XmlAttribute attribute in commentaryNode.Attributes)
                        {
                            switch (attribute.Name)
                            {
                                case "raceId":
                                    raceId = Convert.ToInt32(attribute.InnerText);
                                    break;
                                case "sessionId":
                                    sessionId = Convert.ToInt32(attribute.InnerText);
                                    break;
                                case "timestamp":
                                    timeStamp = DateTime.ParseExact(attribute.InnerText, "yyyyMMddHHmm", CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                                    break;
                                case "title":
                                    string title = attribute.InnerText.ToString().ToLower();
                                    if (title.IndexOf("superbike") >= 0)
                                    {
                                        competition = "SuperBikes";
                                    }
                                    else if (title.IndexOf("formula") >= 0 || title.IndexOf("f1") >= 0)
                                    {
                                        competition = "Formula1";
                                    }
                                    else if (title.IndexOf("motogp") >= 0)
                                    {
                                        competition = "MotoGP";
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append("DELETE FROM tennis.dbo.motorsport_commentary WHERE (competition = '" + competition + "' AND race = " + raceId + " AND session = " + sessionId + " AND year = " + DateTime.Now.Year + ");");
                        foreach (XmlNode lineNode in commentaryNode.SelectNodes("line"))
                        {
                            int count = 0;
                            string time = string.Empty;
                            string text = string.Empty;

                            foreach (XmlAttribute attribute in lineNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "count":
                                        count = Convert.ToInt32(attribute.InnerText);
                                        break;
                                    default:
                                        break;
                                }
                            }

                            foreach (XmlNode detailNode in lineNode.ChildNodes)
                            {
                                switch (detailNode.Name)
                                {
                                    case "laportime":
                                        time = detailNode.InnerText;
                                        break;
                                    case "text":
                                        text = detailNode.InnerText;
                                        break;
                                    default:
                                        break;
                                }
                            }

                            sb.Append("INSERT INTO tennis.dbo.motorsport_commentary (competition, race, session, year, row, time, text, timestamp, created) VALUES ('" + competition + "', " + raceId + ", " + sessionId + ", " + DateTime.Now.Year + ", " + count + ", '" + time.Replace("'", "''") + "', '" + text.Replace("'", "''") + "', '" + timeStamp + "', GetDate());");
                        }

                        if (!String.IsNullOrEmpty(sb.ToString()))
                        {
                            SqlQuery = new SqlCommand(sb.ToString(), dbConn);
                            SqlQuery.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                processed = false;
                _logger.Error("Error occurred in Commentary()", ex);
                curRun.Errors.Add("Commentary - " + fileName + " - " + ex.Message);
            }

            return processed;
        }

        private bool stageDetails(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                DateTime startdate = DateTime.Now;
                DateTime endDate = DateTime.Now;
                string Country = "";
                string Circuit = "";
                string startTime = "";
                string endTime = "";
                DateTime Date = new DateTime(1900, 1, 1);
                int noLaps = -1;

                foreach (XmlAttribute SubAttribute in tmpNode.Attributes)
                {
                    switch (SubAttribute.Name)
                    {
                        case "startTime":
                            startTime = SubAttribute.Value.ToString();
                            break;
                        case "endTime":
                            endTime = SubAttribute.Value.ToString();
                            break;
                        case "country":
                            Country = SubAttribute.Value.ToString();
                            break;
                        case "circuit":
                            Circuit = SubAttribute.Value.ToString();
                            break;
                        case "date":
                            Date = new DateTime(Convert.ToInt32(SubAttribute.Value.Replace("/", "-").Split('-')[2]), Convert.ToInt32(SubAttribute.Value.Replace("/", "-").Split('-')[1]), Convert.ToInt32(SubAttribute.Value.Replace("/", "-").Split('-')[0]));
                            break;
                        case "noLaps":
                            noLaps = Convert.ToInt32(SubAttribute.Value.ToString());
                            break;
                        default:
                            break;
                    }
                }

                SqlQuery = new SqlCommand("Select Count(Id) From tennis.dbo.pa_f1Stages Where (roundId = @Round And stageId = @Stage And Year(Date) = @Year)", dbConn);
                SqlQuery.Parameters.Add("@Year", SqlDbType.Int).Value = Date.Year;
                SqlQuery.Parameters.Add("@Round", SqlDbType.Int).Value = roundId;
                SqlQuery.Parameters.Add("@Stage", SqlDbType.Int).Value = stageId;
                int Present = Convert.ToInt32(SqlQuery.ExecuteScalar());


                if (Present > 0)
                {
                    SqlQuery = new SqlCommand("Update tennis.dbo.pa_f1Stages Set eventId = @eventId, roundId = @Round, roundName = @roundName, stageId = @Stage, stageName = @stageName, country = @country, circuit = @circuit, date = @date, noLaps = @noLaps, timestamp = @timestamp, startTime = @startTime, endTime = @endTime Where (Year(Date) = @Year And roundId = @Round And stageId = @Stage)", dbConn);
                }
                else
                {
                    SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1Stages (eventId, roundId, roundName, stageId, stageName, country, circuit, date, noLaps, timestamp, startTime, endTime) Values (@eventId, @Round, @roundName, @Stage, @stageName, @country, @circuit, @date, @noLaps, @timestamp, @startTime, @endTime)", dbConn);
                }
                SqlQuery.Parameters.Add("@Year", SqlDbType.Int).Value = startdate.Year;
                SqlQuery.Parameters.Add("@Round", SqlDbType.Int).Value = roundId;
                SqlQuery.Parameters.Add("@Stage", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@noLaps", SqlDbType.Int).Value = noLaps;
                SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                SqlQuery.Parameters.Add("@country", SqlDbType.VarChar).Value = Country;
                SqlQuery.Parameters.Add("@circuit", SqlDbType.VarChar).Value = Circuit;
                SqlQuery.Parameters.Add("@startTime", SqlDbType.VarChar).Value = startTime;
                SqlQuery.Parameters.Add("@endTime", SqlDbType.VarChar).Value = endTime;
                SqlQuery.Parameters.Add("@seasonStartDate", SqlDbType.DateTime).Value = startdate;
                SqlQuery.Parameters.Add("@seasonEndDate", SqlDbType.DateTime).Value = endDate;
                SqlQuery.Parameters.Add("@date", SqlDbType.DateTime).Value = Date;
                SqlQuery.Parameters.Add("@timestamp", SqlDbType.DateTime).Value = DateTime.Now;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in stageDetails()", ex);
                curRun.Errors.Add("Stage details - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool Drivers(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_f1Drivers Where (eventId = @eventId And stageId = @stageId And roundId = @roundId)", dbConn);
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.ExecuteNonQuery();

                foreach (XmlNode MainNode in tmpNode.SelectNodes("Driver"))
                {
                    int driverId = -1;
                    int teamId = -1;
                    string carNo = "";
                    string firstName = "";
                    string middleNames = "";
                    string lastName = "";
                    string initials = "";
                    string abbreviation = "";
                    string fullname = "";
                    string teamName = "";
                    string carSponsor = "";
                    string carEngine = "";
                    string carTyre = "";

                    foreach (XmlAttribute driverAttribute in MainNode.Attributes)
                    {
                        switch (driverAttribute.Name)
                        {
                            case "driverId":
                                driverId = Convert.ToInt32(driverAttribute.Value.ToString());
                                break;
                            case "carNo":
                                carNo = driverAttribute.Value.ToString();
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode nameNode in MainNode.SelectNodes("Name"))
                    {
                        fullname = nameNode.InnerText;
                        foreach (XmlAttribute nameAttribute in nameNode.Attributes)
                        {
                            switch (nameAttribute.Name)
                            {
                                case "firstName":
                                    firstName = nameAttribute.Value.ToString();
                                    break;
                                case "middleNames":
                                    middleNames = nameAttribute.Value.ToString();
                                    break;
                                case "lastName":
                                    lastName = nameAttribute.Value.ToString();
                                    break;
                                case "initials":
                                    initials = nameAttribute.Value.ToString();
                                    break;
                                case "abbreviation":
                                    abbreviation = nameAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    foreach (XmlNode teamNode in MainNode.SelectNodes("Team"))
                    {
                        foreach (XmlAttribute nameAttribute in teamNode.Attributes)
                        {
                            switch (nameAttribute.Name)
                            {
                                case "teamId":
                                    teamId = Convert.ToInt32(nameAttribute.Value.ToString());
                                    break;
                                case "teamName":
                                    teamName = nameAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode carNode in teamNode.SelectNodes("VehicleDetails"))
                        {
                            foreach (XmlAttribute carAttribute in carNode.Attributes)
                            {
                                switch (carAttribute.Name)
                                {
                                    case "sponsor":
                                        carSponsor = carAttribute.Value.ToString();
                                        break;
                                    case "engineManufacturer":
                                        carEngine = carAttribute.Value.ToString();
                                        break;
                                    case "tyreManufacturer":
                                        carTyre = carAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1Drivers (driverId, eventId, roundId, roundName, stageId, stageName, carNo, firstname, lastname, initials, abbreviation, fullname, teamid, teamname, carsponsor, carengine, cartyre, middlename) Values (@driverId, @eventId, @roundId, @roundName, @stageId, @stageName, @carNo, @firstname, @lastname, @initials, @abbreviation, @fullname, @teamid, @teamname, @carsponsor, @carengine, @cartyre, @middlename)", dbConn);
                    SqlQuery.Parameters.Add("@driverId", SqlDbType.Int).Value = driverId;
                    SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                    SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                    SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                    SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                    SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                    SqlQuery.Parameters.Add("@carNo", SqlDbType.VarChar).Value = carNo;
                    SqlQuery.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstName;
                    SqlQuery.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastName;
                    SqlQuery.Parameters.Add("@initials", SqlDbType.VarChar).Value = initials;
                    SqlQuery.Parameters.Add("@abbreviation", SqlDbType.VarChar).Value = abbreviation;
                    SqlQuery.Parameters.Add("@fullname", SqlDbType.VarChar).Value = fullname;
                    SqlQuery.Parameters.Add("@teamid", SqlDbType.Int).Value = teamId;
                    SqlQuery.Parameters.Add("@teamname", SqlDbType.VarChar).Value = teamName;
                    SqlQuery.Parameters.Add("@carsponsor", SqlDbType.VarChar).Value = carSponsor;
                    SqlQuery.Parameters.Add("@carengine", SqlDbType.VarChar).Value = carEngine;
                    SqlQuery.Parameters.Add("@cartyre", SqlDbType.VarChar).Value = carTyre;
                    SqlQuery.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middleNames;
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in Drivers()", ex);
                curRun.Errors.Add("Drivers - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool Results(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;
            string Status = "";

            try
            {
                SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_f1Results Where (eventId = @eventId And stageId = @stageId And roundId = @roundId)", dbConn);
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.ExecuteNonQuery();

                foreach (XmlAttribute resultAttribute in tmpNode.Attributes)
                {
                    switch (resultAttribute.Name)
                    {
                        case "resultStatus":
                            Status = resultAttribute.Value.ToString();
                            break;
                        default:
                            break;
                    }
                }

                foreach (XmlNode MainNode in tmpNode.SelectNodes("DriverResult"))
                {
                    int Rank = -1;
                    int driverId = -1;
                    int teamId = -1;
                    int Incomplete = 0;
                    int incompleteLaps = -1;
                    int fastestLap = -1;
                    string carNo = "";
                    string firstName = "";
                    string middleNames = "";
                    string lastName = "";
                    string initials = "";
                    string abbreviation = "";
                    string fullname = "";
                    string teamName = "";
                    string carSponsor = "";
                    string carEngine = "";
                    string carTyre = "";
                    string gapToLeader = "";
                    string gapToCarInFront = "";
                    string raceTime = "";
                    string incompleteReason = "";
                    string FastestLapTime = "";
                    double points = 0.0;

                    foreach (XmlAttribute mainAttribute in MainNode.Attributes)
                    {
                        switch (mainAttribute.Name)
                        {
                            case "rank":
                                Rank = Convert.ToInt32(mainAttribute.Value.ToString());
                                break;
                            case "gapToLeader":
                                gapToLeader = mainAttribute.Value.ToString();
                                break;
                            case "gapToCarInFront":
                                gapToCarInFront = mainAttribute.Value.ToString();
                                break;
                            case "raceTime":
                                raceTime = mainAttribute.Value.ToString();
                                break;
                            case "points":
                                points = Convert.ToDouble(mainAttribute.Value.ToString());
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode fastestNode in MainNode.SelectNodes("DriverFastestLap"))
                    {
                        foreach (XmlAttribute fastestAttribute in fastestNode.Attributes)
                        {
                            switch (fastestAttribute.Name)
                            {
                                case "lapNo":
                                    bool result = Int32.TryParse(fastestAttribute.Value.ToString(), out fastestLap);
                                    break;
                                case "lapTime":
                                    FastestLapTime = fastestAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    foreach (XmlNode incompleteNode in MainNode.SelectNodes("Incomplete"))
                    {
                        Incomplete = 1;

                        foreach (XmlAttribute incompleteAttribute in incompleteNode.Attributes)
                        {
                            switch (incompleteAttribute.Name)
                            {
                                case "lapsCompleted":
                                    bool result = Int32.TryParse(incompleteAttribute.Value.ToString(), out incompleteLaps);
                                    break;
                                case "reason":
                                    incompleteReason = incompleteAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    foreach (XmlNode incompleteNode in MainNode.SelectNodes("Disqualification"))
                    {
                        Incomplete = 1;
                        incompleteReason = "Disqualified";
                        foreach (XmlAttribute incompleteAttribute in incompleteNode.Attributes)
                        {
                            switch (incompleteAttribute.Name)
                            {
                                case "lapsCompleted":
                                    bool result = Int32.TryParse(incompleteAttribute.Value.ToString(), out incompleteLaps);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    foreach (XmlNode driverNode in MainNode.SelectNodes("Driver"))
                    {
                        foreach (XmlAttribute driverAttribute in driverNode.Attributes)
                        {
                            switch (driverAttribute.Name)
                            {
                                case "driverId":
                                    driverId = Convert.ToInt32(driverAttribute.Value.ToString());
                                    break;
                                case "carNo":
                                    carNo = driverAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode nameNode in driverNode.SelectNodes("Name"))
                        {
                            fullname = nameNode.InnerText;
                            foreach (XmlAttribute nameAttribute in nameNode.Attributes)
                            {
                                switch (nameAttribute.Name)
                                {
                                    case "firstName":
                                        firstName = nameAttribute.Value.ToString();
                                        break;
                                    case "middleNames":
                                        middleNames = nameAttribute.Value.ToString();
                                        break;
                                    case "lastName":
                                        lastName = nameAttribute.Value.ToString();
                                        break;
                                    case "initials":
                                        initials = nameAttribute.Value.ToString();
                                        break;
                                    case "abbreviation":
                                        abbreviation = nameAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        foreach (XmlNode teamNode in driverNode.SelectNodes("Team"))
                        {
                            foreach (XmlAttribute nameAttribute in teamNode.Attributes)
                            {
                                switch (nameAttribute.Name)
                                {
                                    case "teamId":
                                        teamId = Convert.ToInt32(nameAttribute.Value.ToString());
                                        break;
                                    case "teamName":
                                        teamName = nameAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            foreach (XmlNode carNode in teamNode.SelectNodes("VehicleDetails"))
                            {
                                foreach (XmlAttribute carAttribute in carNode.Attributes)
                                {
                                    switch (carAttribute.Name)
                                    {
                                        case "sponsor":
                                            carSponsor = carAttribute.Value.ToString();
                                            break;
                                        case "engineManufacturer":
                                            carEngine = carAttribute.Value.ToString();
                                            break;
                                        case "tyreManufacturer":
                                            carTyre = carAttribute.Value.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1Results (driverId, eventId, roundId, roundName, stageId, stageName, carNo, firstname, lastname, initials, abbreviation, fullname, teamid, teamname, carsponsor, carengine, cartyre, middlename, status, rank, gapToLeader, gapToCarInFront, Points, raceTime, incomplete, incompleteReason, incompleteLaps, fastestLapTime, fastestLap) Values (@driverId, @eventId, @roundId, @roundName, @stageId, @stageName, @carNo, @firstname, @lastname, @initials, @abbreviation, @fullname, @teamid, @teamname, @carsponsor, @carengine, @cartyre, @middlename, @status, @rank, @gapToLeader, @gapToCarInFront, @Points, @raceTime, @incomplete, @incompleteReason, @incompleteLaps, @fastestLapTime, @fastestLap)", dbConn);
                    SqlQuery.Parameters.Add("@driverId", SqlDbType.Int).Value = driverId;
                    SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                    SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                    SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                    SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                    SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                    SqlQuery.Parameters.Add("@carNo", SqlDbType.VarChar).Value = carNo;
                    SqlQuery.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstName;
                    SqlQuery.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastName;
                    SqlQuery.Parameters.Add("@initials", SqlDbType.VarChar).Value = initials;
                    SqlQuery.Parameters.Add("@abbreviation", SqlDbType.VarChar).Value = abbreviation;
                    SqlQuery.Parameters.Add("@fullname", SqlDbType.VarChar).Value = fullname;
                    SqlQuery.Parameters.Add("@teamid", SqlDbType.Int).Value = teamId;
                    SqlQuery.Parameters.Add("@teamname", SqlDbType.VarChar).Value = teamName;
                    SqlQuery.Parameters.Add("@carsponsor", SqlDbType.VarChar).Value = carSponsor;
                    SqlQuery.Parameters.Add("@carengine", SqlDbType.VarChar).Value = carEngine;
                    SqlQuery.Parameters.Add("@cartyre", SqlDbType.VarChar).Value = carTyre;
                    SqlQuery.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middleNames;
                    SqlQuery.Parameters.Add("@status", SqlDbType.VarChar).Value = Status;
                    SqlQuery.Parameters.Add("@rank", SqlDbType.VarChar).Value = Rank;
                    SqlQuery.Parameters.Add("@gapToLeader", SqlDbType.VarChar).Value = gapToLeader;
                    SqlQuery.Parameters.Add("@gapToCarInFront", SqlDbType.VarChar).Value = gapToCarInFront;
                    SqlQuery.Parameters.Add("@Points", SqlDbType.VarChar).Value = points;
                    SqlQuery.Parameters.Add("@raceTime", SqlDbType.VarChar).Value = raceTime;
                    SqlQuery.Parameters.Add("@incomplete", SqlDbType.Int).Value = Incomplete;
                    SqlQuery.Parameters.Add("@incompleteReason", SqlDbType.VarChar).Value = incompleteReason;
                    SqlQuery.Parameters.Add("@incompleteLaps", SqlDbType.Int).Value = incompleteLaps;
                    SqlQuery.Parameters.Add("@fastestLapTime", SqlDbType.VarChar).Value = FastestLapTime;
                    SqlQuery.Parameters.Add("@fastestLap", SqlDbType.Int).Value = fastestLap;
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in Results()", ex);
                curRun.Errors.Add("Results - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool fastestLap(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                int lapNumber = -1;
                int driverId = -1;
                int teamId = -1;
                string carNo = "";
                string firstName = "";
                string middleNames = "";
                string lastName = "";
                string initials = "";
                string abbreviation = "";
                string fullname = "";
                string teamName = "";
                string carSponsor = "";
                string carEngine = "";
                string carTyre = "";
                string lapTime = "";

                foreach (XmlAttribute fatestAttribute in tmpNode.Attributes)
                {
                    switch (fatestAttribute.Name)
                    {
                        case "lapNo":
                            bool result = Int32.TryParse(fatestAttribute.Value.ToString(), out lapNumber);
                            break;
                        case "time":
                            lapTime = fatestAttribute.Value.ToString();
                            break;
                        default:
                            break;
                    }
                }

                foreach (XmlNode driverNode in tmpNode.SelectNodes("Driver"))
                {
                    foreach (XmlAttribute driverAttribute in driverNode.Attributes)
                    {
                        switch (driverAttribute.Name)
                        {
                            case "driverId":
                                driverId = Convert.ToInt32(driverAttribute.Value.ToString());
                                break;
                            case "carNo":
                                carNo = driverAttribute.Value.ToString();
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode nameNode in driverNode.SelectNodes("Name"))
                    {
                        fullname = nameNode.InnerText;
                        foreach (XmlAttribute nameAttribute in nameNode.Attributes)
                        {
                            switch (nameAttribute.Name)
                            {
                                case "firstName":
                                    firstName = nameAttribute.Value.ToString();
                                    break;
                                case "middleNames":
                                    middleNames = nameAttribute.Value.ToString();
                                    break;
                                case "lastName":
                                    lastName = nameAttribute.Value.ToString();
                                    break;
                                case "initials":
                                    initials = nameAttribute.Value.ToString();
                                    break;
                                case "abbreviation":
                                    abbreviation = nameAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    foreach (XmlNode teamNode in driverNode.SelectNodes("Team"))
                    {
                        foreach (XmlAttribute nameAttribute in teamNode.Attributes)
                        {
                            switch (nameAttribute.Name)
                            {
                                case "teamId":
                                    teamId = Convert.ToInt32(nameAttribute.Value.ToString());
                                    break;
                                case "teamName":
                                    teamName = nameAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode carNode in teamNode.SelectNodes("VehicleDetails"))
                        {
                            foreach (XmlAttribute carAttribute in carNode.Attributes)
                            {
                                switch (carAttribute.Name)
                                {
                                    case "sponsor":
                                        carSponsor = carAttribute.Value.ToString();
                                        break;
                                    case "engineManufacturer":
                                        carEngine = carAttribute.Value.ToString();
                                        break;
                                    case "tyreManufacturer":
                                        carTyre = carAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1fastestLaps (driverId, eventId, roundId, roundName, stageId, stageName, carNo, firstname, lastname, initials, abbreviation, fullname, teamid, teamname, carsponsor, carengine, cartyre, middlename, lapNo, lapTime) Values (@driverId, @eventId, @roundId, @roundName, @stageId, @stageName, @carNo, @firstname, @lastname, @initials, @abbreviation, @fullname, @teamid, @teamname, @carsponsor, @carengine, @cartyre, @middlename, @lapNo, @lapTime)", dbConn);
                    SqlQuery.Parameters.Add("@driverId", SqlDbType.Int).Value = driverId;
                    SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                    SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                    SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                    SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                    SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                    SqlQuery.Parameters.Add("@carNo", SqlDbType.VarChar).Value = carNo;
                    SqlQuery.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstName;
                    SqlQuery.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastName;
                    SqlQuery.Parameters.Add("@initials", SqlDbType.VarChar).Value = initials;
                    SqlQuery.Parameters.Add("@abbreviation", SqlDbType.VarChar).Value = abbreviation;
                    SqlQuery.Parameters.Add("@fullname", SqlDbType.VarChar).Value = fullname;
                    SqlQuery.Parameters.Add("@teamid", SqlDbType.Int).Value = teamId;
                    SqlQuery.Parameters.Add("@teamname", SqlDbType.VarChar).Value = teamName;
                    SqlQuery.Parameters.Add("@carsponsor", SqlDbType.VarChar).Value = carSponsor;
                    SqlQuery.Parameters.Add("@carengine", SqlDbType.VarChar).Value = carEngine;
                    SqlQuery.Parameters.Add("@cartyre", SqlDbType.VarChar).Value = carTyre;
                    SqlQuery.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middleNames;
                    SqlQuery.Parameters.Add("@lapNo", SqlDbType.Int).Value = lapNumber;
                    SqlQuery.Parameters.Add("@lapTime", SqlDbType.VarChar).Value = lapTime;
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in fastestLap()", ex);
                curRun.Errors.Add("Fastest lap - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool Leaderboards(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;
            int lapNumber = -1;

            try
            {
                SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_f1Leaderboards Where (eventId = @eventId And stageId = @stageId And roundId = @roundId)", dbConn);
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.ExecuteNonQuery();

                foreach (XmlAttribute leaderboardAttribute in tmpNode.Attributes)
                {
                    switch (leaderboardAttribute.Name)
                    {
                        case "lapNo":
                            bool result = Int32.TryParse(leaderboardAttribute.Value.ToString(), out lapNumber);
                            break;
                        default:
                            break;
                    }
                }

                foreach (XmlNode MainNode in tmpNode.SelectNodes("DriverPosition"))
                {
                    int Rank = -1;
                    int driverId = -1;
                    int teamId = -1;
                    string carNo = "";
                    string firstName = "";
                    string middleNames = "";
                    string lastName = "";
                    string initials = "";
                    string abbreviation = "";
                    string fullname = "";
                    string teamName = "";
                    string carSponsor = "";
                    string carEngine = "";
                    string carTyre = "";
                    string gapToLeader = "";
                    string gapToCarInFront = "";

                    foreach (XmlAttribute mainAttribute in MainNode.Attributes)
                    {
                        switch (mainAttribute.Name)
                        {
                            case "position":
                                Rank = Convert.ToInt32(mainAttribute.Value.ToString());
                                break;
                            case "gapToLeader":
                                gapToLeader = mainAttribute.Value.ToString();
                                break;
                            case "gapToCarInFront":
                                gapToCarInFront = mainAttribute.Value.ToString();
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode driverNode in MainNode.SelectNodes("Driver"))
                    {
                        foreach (XmlAttribute driverAttribute in driverNode.Attributes)
                        {
                            switch (driverAttribute.Name)
                            {
                                case "driverId":
                                    driverId = Convert.ToInt32(driverAttribute.Value.ToString());
                                    break;
                                case "carNo":
                                    carNo = driverAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode nameNode in driverNode.SelectNodes("Name"))
                        {
                            fullname = nameNode.InnerText;
                            foreach (XmlAttribute nameAttribute in nameNode.Attributes)
                            {
                                switch (nameAttribute.Name)
                                {
                                    case "firstName":
                                        firstName = nameAttribute.Value.ToString();
                                        break;
                                    case "middleNames":
                                        middleNames = nameAttribute.Value.ToString();
                                        break;
                                    case "lastName":
                                        lastName = nameAttribute.Value.ToString();
                                        break;
                                    case "initials":
                                        initials = nameAttribute.Value.ToString();
                                        break;
                                    case "abbreviation":
                                        abbreviation = nameAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        foreach (XmlNode teamNode in driverNode.SelectNodes("Team"))
                        {
                            foreach (XmlAttribute nameAttribute in teamNode.Attributes)
                            {
                                switch (nameAttribute.Name)
                                {
                                    case "teamId":
                                        teamId = Convert.ToInt32(nameAttribute.Value.ToString());
                                        break;
                                    case "teamName":
                                        teamName = nameAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            foreach (XmlNode carNode in teamNode.SelectNodes("VehicleDetails"))
                            {
                                foreach (XmlAttribute carAttribute in carNode.Attributes)
                                {
                                    switch (carAttribute.Name)
                                    {
                                        case "sponsor":
                                            carSponsor = carAttribute.Value.ToString();
                                            break;
                                        case "engineManufacturer":
                                            carEngine = carAttribute.Value.ToString();
                                            break;
                                        case "tyreManufacturer":
                                            carTyre = carAttribute.Value.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1Leaderboards (driverId, eventId, roundId, roundName, stageId, stageName, carNo, firstname, lastname, initials, abbreviation, fullname, teamid, teamname, carsponsor, carengine, cartyre, middlename, rank, gapToLeader, gapToCarInFront, leadersLap) Values (@driverId, @eventId, @roundId, @roundName, @stageId, @stageName, @carNo, @firstname, @lastname, @initials, @abbreviation, @fullname, @teamid, @teamname, @carsponsor, @carengine, @cartyre, @middlename, @rank, @gapToLeader, @gapToCarInFront, @leadersLap)", dbConn);
                    SqlQuery.Parameters.Add("@driverId", SqlDbType.Int).Value = driverId;
                    SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                    SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                    SqlQuery.Parameters.Add("@leadersLap", SqlDbType.Int).Value = lapNumber;
                    SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                    SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                    SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                    SqlQuery.Parameters.Add("@carNo", SqlDbType.VarChar).Value = carNo;
                    SqlQuery.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstName;
                    SqlQuery.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastName;
                    SqlQuery.Parameters.Add("@initials", SqlDbType.VarChar).Value = initials;
                    SqlQuery.Parameters.Add("@abbreviation", SqlDbType.VarChar).Value = abbreviation;
                    SqlQuery.Parameters.Add("@fullname", SqlDbType.VarChar).Value = fullname;
                    SqlQuery.Parameters.Add("@teamid", SqlDbType.Int).Value = teamId;
                    SqlQuery.Parameters.Add("@teamname", SqlDbType.VarChar).Value = teamName;
                    SqlQuery.Parameters.Add("@carsponsor", SqlDbType.VarChar).Value = carSponsor;
                    SqlQuery.Parameters.Add("@carengine", SqlDbType.VarChar).Value = carEngine;
                    SqlQuery.Parameters.Add("@cartyre", SqlDbType.VarChar).Value = carTyre;
                    SqlQuery.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middleNames;
                    SqlQuery.Parameters.Add("@rank", SqlDbType.VarChar).Value = Rank;
                    SqlQuery.Parameters.Add("@gapToLeader", SqlDbType.VarChar).Value = gapToLeader;
                    SqlQuery.Parameters.Add("@gapToCarInFront", SqlDbType.VarChar).Value = gapToCarInFront;
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in Leaderboards()", ex);
                curRun.Errors.Add("Leaderboard - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool driverDetails(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                int driverId = -1;
                int teamId = -1;
                string carNo = "";
                string firstName = "";
                string middleNames = "";
                string lastName = "";
                string initials = "";
                string abbreviation = "";
                string fullname = "";
                string teamName = "";
                string carSponsor = "";
                string carEngine = "";
                string carTyre = "";
                string type = "";
                string reason = "";
                int lapNumber = -1;
                string extraDetail = "";

                foreach (XmlNode incidentNode in tmpNode.SelectNodes("Incident"))
                {
                    foreach (XmlAttribute incidentAttribute in incidentNode.Attributes)
                    {
                        switch (incidentAttribute.Name)
                        {
                            case "type":
                                type = incidentAttribute.Value.ToString();
                                break;
                            case "reason":
                                reason = incidentAttribute.Value.ToString();
                                break;
                            case "additionalInfo":
                                extraDetail = incidentAttribute.Value.ToString();
                                break;
                            case "lapNo":
                                bool result = Int32.TryParse(incidentAttribute.Value.ToString(), out lapNumber);
                                break;
                            default:
                                break;
                        }
                    }
                }

                foreach (XmlNode driverNode in tmpNode.SelectNodes("Driver"))
                {
                    foreach (XmlAttribute driverAttribute in driverNode.Attributes)
                    {
                        switch (driverAttribute.Name)
                        {
                            case "driverId":
                                driverId = Convert.ToInt32(driverAttribute.Value.ToString());
                                break;
                            case "carNo":
                                carNo = driverAttribute.Value.ToString();
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode nameNode in driverNode.SelectNodes("Name"))
                    {
                        fullname = nameNode.InnerText;
                        foreach (XmlAttribute nameAttribute in nameNode.Attributes)
                        {
                            switch (nameAttribute.Name)
                            {
                                case "firstName":
                                    firstName = nameAttribute.Value.ToString();
                                    break;
                                case "middleNames":
                                    middleNames = nameAttribute.Value.ToString();
                                    break;
                                case "lastName":
                                    lastName = nameAttribute.Value.ToString();
                                    break;
                                case "initials":
                                    initials = nameAttribute.Value.ToString();
                                    break;
                                case "abbreviation":
                                    abbreviation = nameAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    foreach (XmlNode teamNode in driverNode.SelectNodes("Team"))
                    {
                        foreach (XmlAttribute nameAttribute in teamNode.Attributes)
                        {
                            switch (nameAttribute.Name)
                            {
                                case "teamId":
                                    teamId = Convert.ToInt32(nameAttribute.Value.ToString());
                                    break;
                                case "teamName":
                                    teamName = nameAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode carNode in teamNode.SelectNodes("VehicleDetails"))
                        {
                            foreach (XmlAttribute carAttribute in carNode.Attributes)
                            {
                                switch (carAttribute.Name)
                                {
                                    case "sponsor":
                                        carSponsor = carAttribute.Value.ToString();
                                        break;
                                    case "engineManufacturer":
                                        carEngine = carAttribute.Value.ToString();
                                        break;
                                    case "tyreManufacturer":
                                        carTyre = carAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }
                }

                SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1driverDetails (driverId, eventId, roundId, roundName, stageId, stageName, carNo, firstname, lastname, initials, abbreviation, fullname, teamid, teamname, carsponsor, carengine, cartyre, middlename, type, reason, extraDetail, lapNo) Values (@driverId, @eventId, @roundId, @roundName, @stageId, @stageName, @carNo, @firstname, @lastname, @initials, @abbreviation, @fullname, @teamid, @teamname, @carsponsor, @carengine, @cartyre, @middlename, @type, @reason, @extraDetail, @lapNo)", dbConn);
                SqlQuery.Parameters.Add("@driverId", SqlDbType.Int).Value = driverId;
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.Parameters.Add("@lapNo", SqlDbType.Int).Value = lapNumber;
                SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                SqlQuery.Parameters.Add("@carNo", SqlDbType.VarChar).Value = carNo;
                SqlQuery.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstName;
                SqlQuery.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastName;
                SqlQuery.Parameters.Add("@initials", SqlDbType.VarChar).Value = initials;
                SqlQuery.Parameters.Add("@abbreviation", SqlDbType.VarChar).Value = abbreviation;
                SqlQuery.Parameters.Add("@fullname", SqlDbType.VarChar).Value = fullname;
                SqlQuery.Parameters.Add("@teamid", SqlDbType.Int).Value = teamId;
                SqlQuery.Parameters.Add("@teamname", SqlDbType.VarChar).Value = teamName;
                SqlQuery.Parameters.Add("@carsponsor", SqlDbType.VarChar).Value = carSponsor;
                SqlQuery.Parameters.Add("@carengine", SqlDbType.VarChar).Value = carEngine;
                SqlQuery.Parameters.Add("@cartyre", SqlDbType.VarChar).Value = carTyre;
                SqlQuery.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middleNames;
                SqlQuery.Parameters.Add("@type", SqlDbType.VarChar).Value = type;
                SqlQuery.Parameters.Add("@reason", SqlDbType.VarChar).Value = reason;
                SqlQuery.Parameters.Add("@extraDetail", SqlDbType.VarChar).Value = extraDetail;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in driverDetails()", ex);
                curRun.Errors.Add("Driver details - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool status(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                string raceStatus = "";
                string raceStatusComment = "";

                foreach (XmlAttribute statusAttribute in tmpNode.Attributes)
                {
                    switch (statusAttribute.Name)
                    {
                        case "statusDesc":
                            raceStatus = statusAttribute.Value.ToString();
                            break;
                        case "comment":
                            raceStatusComment = statusAttribute.Value.ToString();
                            break;
                        default:
                            break;
                    }
                }

                SqlQuery = new SqlCommand("Update tennis.dbo.pa_f1Stages Set raceStatus = @raceStatus, raceStatusComments = @raceStatusComments Where (eventId = @eventId And stageId = @stageId And roundId = @roundId)", dbConn);
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                SqlQuery.Parameters.Add("@raceStatus", SqlDbType.VarChar).Value = raceStatus;
                SqlQuery.Parameters.Add("@raceStatusComments", SqlDbType.VarChar).Value = raceStatusComment;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in status()", ex);
                curRun.Errors.Add("Status - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool weather(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                string description = "";

                foreach (XmlAttribute weatherAttribute in tmpNode.Attributes)
                {
                    switch (weatherAttribute.Name)
                    {
                        case "description":
                            description = weatherAttribute.Value.ToString();
                            break;
                        default:
                            break;
                    }
                }

                SqlQuery = new SqlCommand("Update tennis.dbo.pa_f1Stages Set raceWeather = @weather Where (eventId = @eventId And stageId = @stageId And roundId = @roundId)", dbConn);
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                SqlQuery.Parameters.Add("@weather", SqlDbType.VarChar).Value = description;
                SqlQuery.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in weather()", ex);
                curRun.Errors.Add("Weather - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool startingGrid(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_f1StartingGrid Where (eventId = @eventId And stageId = @stageId And roundId = @roundId)", dbConn);
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.ExecuteNonQuery();

                foreach (XmlNode MainNode in tmpNode.SelectNodes("StartingPosition"))
                {
                    int Rank = -1;
                    int driverId = -1;
                    int teamId = -1;
                    int startInPit = 0;
                    string carNo = "";
                    string firstName = "";
                    string middleNames = "";
                    string lastName = "";
                    string initials = "";
                    string abbreviation = "";
                    string fullname = "";
                    string teamName = "";
                    string carSponsor = "";
                    string carEngine = "";
                    string carTyre = "";
                    string raceTime = "";

                    foreach (XmlAttribute mainAttribute in MainNode.Attributes)
                    {
                        switch (mainAttribute.Name)
                        {
                            case "position":
                                Rank = Convert.ToInt32(mainAttribute.Value.ToString());
                                break;
                            case "qualifyingTime":
                                raceTime = mainAttribute.Value.ToString();
                                break;
                            case "startingInPit":
                                if (mainAttribute.Value.ToString() == "Yes")
                                {
                                    startInPit = 1;
                                }
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode driverNode in MainNode.SelectNodes("Driver"))
                    {
                        foreach (XmlAttribute driverAttribute in driverNode.Attributes)
                        {
                            switch (driverAttribute.Name)
                            {
                                case "driverId":
                                    driverId = Convert.ToInt32(driverAttribute.Value.ToString());
                                    break;
                                case "carNo":
                                    carNo = driverAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode nameNode in driverNode.SelectNodes("Name"))
                        {
                            fullname = nameNode.InnerText;
                            foreach (XmlAttribute nameAttribute in nameNode.Attributes)
                            {
                                switch (nameAttribute.Name)
                                {
                                    case "firstName":
                                        firstName = nameAttribute.Value.ToString();
                                        break;
                                    case "middleNames":
                                        middleNames = nameAttribute.Value.ToString();
                                        break;
                                    case "lastName":
                                        lastName = nameAttribute.Value.ToString();
                                        break;
                                    case "initials":
                                        initials = nameAttribute.Value.ToString();
                                        break;
                                    case "abbreviation":
                                        abbreviation = nameAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        foreach (XmlNode teamNode in driverNode.SelectNodes("Team"))
                        {
                            foreach (XmlAttribute nameAttribute in teamNode.Attributes)
                            {
                                switch (nameAttribute.Name)
                                {
                                    case "teamId":
                                        teamId = Convert.ToInt32(nameAttribute.Value.ToString());
                                        break;
                                    case "teamName":
                                        teamName = nameAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            foreach (XmlNode carNode in teamNode.SelectNodes("VehicleDetails"))
                            {
                                foreach (XmlAttribute carAttribute in carNode.Attributes)
                                {
                                    switch (carAttribute.Name)
                                    {
                                        case "sponsor":
                                            carSponsor = carAttribute.Value.ToString();
                                            break;
                                        case "engineManufacturer":
                                            carEngine = carAttribute.Value.ToString();
                                            break;
                                        case "tyreManufacturer":
                                            carTyre = carAttribute.Value.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1StartingGrid (driverId, eventId, roundId, roundName, stageId, stageName, carNo, firstname, lastname, initials, abbreviation, fullname, teamid, teamname, carsponsor, carengine, cartyre, middlename, position, qualifyingTime, startInPit) Values (@driverId, @eventId, @roundId, @roundName, @stageId, @stageName, @carNo, @firstname, @lastname, @initials, @abbreviation, @fullname, @teamid, @teamname, @carsponsor, @carengine, @cartyre, @middlename, @rank, @raceTime, @startInPit)", dbConn);
                    SqlQuery.Parameters.Add("@driverId", SqlDbType.Int).Value = driverId;
                    SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                    SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                    SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                    SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                    SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                    SqlQuery.Parameters.Add("@carNo", SqlDbType.VarChar).Value = carNo;
                    SqlQuery.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstName;
                    SqlQuery.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastName;
                    SqlQuery.Parameters.Add("@initials", SqlDbType.VarChar).Value = initials;
                    SqlQuery.Parameters.Add("@abbreviation", SqlDbType.VarChar).Value = abbreviation;
                    SqlQuery.Parameters.Add("@fullname", SqlDbType.VarChar).Value = fullname;
                    SqlQuery.Parameters.Add("@teamid", SqlDbType.Int).Value = teamId;
                    SqlQuery.Parameters.Add("@teamname", SqlDbType.VarChar).Value = teamName;
                    SqlQuery.Parameters.Add("@carsponsor", SqlDbType.VarChar).Value = carSponsor;
                    SqlQuery.Parameters.Add("@carengine", SqlDbType.VarChar).Value = carEngine;
                    SqlQuery.Parameters.Add("@cartyre", SqlDbType.VarChar).Value = carTyre;
                    SqlQuery.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middleNames;
                    SqlQuery.Parameters.Add("@rank", SqlDbType.VarChar).Value = Rank;
                    SqlQuery.Parameters.Add("@raceTime", SqlDbType.VarChar).Value = raceTime;
                    SqlQuery.Parameters.Add("@startInPit", SqlDbType.Int).Value = startInPit;
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in startingGrid()", ex);
                curRun.Errors.Add("Results - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool teamTable(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_f1TeamTable Where (eventId = @eventId And stageId = @stageId And roundId = @roundId)", dbConn);
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.ExecuteNonQuery();

                foreach (XmlNode MainNode in tmpNode.SelectNodes("TeamRank"))
                {
                    int Rank = -1;
                    int teamId = -1;
                    string teamName = "";
                    string carSponsor = "";
                    string carEngine = "";
                    string carTyre = "";
                    double points = 0.0;

                    foreach (XmlAttribute mainAttribute in MainNode.Attributes)
                    {
                        switch (mainAttribute.Name)
                        {
                            case "rank":
                                Rank = Convert.ToInt32(mainAttribute.Value.ToString());
                                break;
                            case "points":
                                points = Convert.ToDouble(mainAttribute.Value.ToString());
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode teamNode in MainNode.SelectNodes("Team"))
                    {
                        foreach (XmlAttribute nameAttribute in teamNode.Attributes)
                        {
                            switch (nameAttribute.Name)
                            {
                                case "teamId":
                                    teamId = Convert.ToInt32(nameAttribute.Value.ToString());
                                    break;
                                case "teamName":
                                    teamName = nameAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode carNode in teamNode.SelectNodes("VehicleDetails"))
                        {
                            foreach (XmlAttribute carAttribute in carNode.Attributes)
                            {
                                switch (carAttribute.Name)
                                {
                                    case "sponsor":
                                        carSponsor = carAttribute.Value.ToString();
                                        break;
                                    case "engineManufacturer":
                                        carEngine = carAttribute.Value.ToString();
                                        break;
                                    case "tyreManufacturer":
                                        carTyre = carAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1TeamTable (eventId, roundId, roundName, stageId, stageName, teamid, teamname, carsponsor, carengine, cartyre, rank, Points) Values (@eventId, @roundId, @roundName, @stageId, @stageName, @teamid, @teamname, @carsponsor, @carengine, @cartyre, @rank, @Points)", dbConn);
                    SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                    SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                    SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                    SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                    SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                    SqlQuery.Parameters.Add("@teamid", SqlDbType.Int).Value = teamId;
                    SqlQuery.Parameters.Add("@teamname", SqlDbType.VarChar).Value = teamName;
                    SqlQuery.Parameters.Add("@carsponsor", SqlDbType.VarChar).Value = carSponsor;
                    SqlQuery.Parameters.Add("@carengine", SqlDbType.VarChar).Value = carEngine;
                    SqlQuery.Parameters.Add("@cartyre", SqlDbType.VarChar).Value = carTyre;
                    SqlQuery.Parameters.Add("@rank", SqlDbType.VarChar).Value = Rank;
                    SqlQuery.Parameters.Add("@Points", SqlDbType.VarChar).Value = points;
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in teamTable()", ex);
                curRun.Errors.Add("Team Table - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private bool driverTable(XmlNode tmpNode, int eventId, int roundId, string roundName, int stageId, string stageName)
        {
            bool Processed = true;
            SqlCommand SqlQuery;

            try
            {
                SqlQuery = new SqlCommand("Delete From tennis.dbo.pa_f1Drivertable Where (eventId = @eventId And stageId = @stageId And roundId = @roundId)", dbConn);
                SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                SqlQuery.ExecuteNonQuery();

                foreach (XmlNode MainNode in tmpNode.SelectNodes("DriverRank"))
                {
                    int Rank = -1;
                    int driverId = -1;
                    int teamId = -1;
                    int Wins = -1;
                    string carNo = "";
                    string firstName = "";
                    string middleNames = "";
                    string lastName = "";
                    string initials = "";
                    string abbreviation = "";
                    string fullname = "";
                    string teamName = "";
                    string carSponsor = "";
                    string carEngine = "";
                    string carTyre = "";
                    double points = 0.0;

                    foreach (XmlAttribute mainAttribute in MainNode.Attributes)
                    {
                        switch (mainAttribute.Name)
                        {
                            case "rank":
                                Rank = Convert.ToInt32(mainAttribute.Value.ToString());
                                break;
                            case "wins":
                                Wins = Convert.ToInt32(mainAttribute.Value.ToString());
                                break;
                            case "points":
                                points = Convert.ToDouble(mainAttribute.Value.ToString());
                                break;
                            default:
                                break;
                        }
                    }

                    foreach (XmlNode driverNode in MainNode.SelectNodes("Driver"))
                    {
                        foreach (XmlAttribute driverAttribute in driverNode.Attributes)
                        {
                            switch (driverAttribute.Name)
                            {
                                case "driverId":
                                    driverId = Convert.ToInt32(driverAttribute.Value.ToString());
                                    break;
                                case "carNo":
                                    carNo = driverAttribute.Value.ToString();
                                    break;
                                default:
                                    break;
                            }
                        }

                        foreach (XmlNode nameNode in driverNode.SelectNodes("Name"))
                        {
                            fullname = nameNode.InnerText;
                            foreach (XmlAttribute nameAttribute in nameNode.Attributes)
                            {
                                switch (nameAttribute.Name)
                                {
                                    case "firstName":
                                        firstName = nameAttribute.Value.ToString();
                                        break;
                                    case "middleNames":
                                        middleNames = nameAttribute.Value.ToString();
                                        break;
                                    case "lastName":
                                        lastName = nameAttribute.Value.ToString();
                                        break;
                                    case "initials":
                                        initials = nameAttribute.Value.ToString();
                                        break;
                                    case "abbreviation":
                                        abbreviation = nameAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }

                        foreach (XmlNode teamNode in driverNode.SelectNodes("Team"))
                        {
                            foreach (XmlAttribute nameAttribute in teamNode.Attributes)
                            {
                                switch (nameAttribute.Name)
                                {
                                    case "teamId":
                                        teamId = Convert.ToInt32(nameAttribute.Value.ToString());
                                        break;
                                    case "teamName":
                                        teamName = nameAttribute.Value.ToString();
                                        break;
                                    default:
                                        break;
                                }
                            }

                            foreach (XmlNode carNode in teamNode.SelectNodes("VehicleDetails"))
                            {
                                foreach (XmlAttribute carAttribute in carNode.Attributes)
                                {
                                    switch (carAttribute.Name)
                                    {
                                        case "sponsor":
                                            carSponsor = carAttribute.Value.ToString();
                                            break;
                                        case "engineManufacturer":
                                            carEngine = carAttribute.Value.ToString();
                                            break;
                                        case "tyreManufacturer":
                                            carTyre = carAttribute.Value.ToString();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }

                    SqlQuery = new SqlCommand("Insert Into tennis.dbo.pa_f1DriverTable (driverId, eventId, roundId, roundName, stageId, stageName, carNo, firstname, lastname, initials, abbreviation, fullname, teamid, teamname, carsponsor, carengine, cartyre, middlename, rank, Points, Wins) Values (@driverId, @eventId, @roundId, @roundName, @stageId, @stageName, @carNo, @firstname, @lastname, @initials, @abbreviation, @fullname, @teamid, @teamname, @carsponsor, @carengine, @cartyre, @middlename, @rank, @Points, @wins)", dbConn);
                    SqlQuery.Parameters.Add("@driverId", SqlDbType.Int).Value = driverId;
                    SqlQuery.Parameters.Add("@eventId", SqlDbType.Int).Value = eventId;
                    SqlQuery.Parameters.Add("@roundId", SqlDbType.Int).Value = roundId;
                    SqlQuery.Parameters.Add("@roundName", SqlDbType.VarChar).Value = roundName;
                    SqlQuery.Parameters.Add("@stageId", SqlDbType.Int).Value = stageId;
                    SqlQuery.Parameters.Add("@stageName", SqlDbType.VarChar).Value = stageName;
                    SqlQuery.Parameters.Add("@carNo", SqlDbType.VarChar).Value = carNo;
                    SqlQuery.Parameters.Add("@firstname", SqlDbType.VarChar).Value = firstName;
                    SqlQuery.Parameters.Add("@lastname", SqlDbType.VarChar).Value = lastName;
                    SqlQuery.Parameters.Add("@initials", SqlDbType.VarChar).Value = initials;
                    SqlQuery.Parameters.Add("@abbreviation", SqlDbType.VarChar).Value = abbreviation;
                    SqlQuery.Parameters.Add("@fullname", SqlDbType.VarChar).Value = fullname;
                    SqlQuery.Parameters.Add("@teamid", SqlDbType.Int).Value = teamId;
                    SqlQuery.Parameters.Add("@teamname", SqlDbType.VarChar).Value = teamName;
                    SqlQuery.Parameters.Add("@carsponsor", SqlDbType.VarChar).Value = carSponsor;
                    SqlQuery.Parameters.Add("@carengine", SqlDbType.VarChar).Value = carEngine;
                    SqlQuery.Parameters.Add("@cartyre", SqlDbType.VarChar).Value = carTyre;
                    SqlQuery.Parameters.Add("@middlename", SqlDbType.VarChar).Value = middleNames;
                    SqlQuery.Parameters.Add("@rank", SqlDbType.VarChar).Value = Rank;
                    SqlQuery.Parameters.Add("@Points", SqlDbType.VarChar).Value = points;
                    SqlQuery.Parameters.Add("@wins", SqlDbType.Int).Value = Wins;
                    SqlQuery.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Processed = false;
                _logger.Error("Error occurred in driverTable()", ex);
                curRun.Errors.Add("Driver Table - " + eventId + " - " + ex.Message);
            }

            return Processed;
        }

        private void archiveFiles()
        {
            if (!(File.Exists("c:/SuperSport/Motorsport/Xml/History/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.ToString("yyyyMMdd") + ".zip")))
            {
                try
                {
                    DiskFolder tmpFolder = new DiskFolder("c:/SuperSport/Motorsport/Xml/Processed");
                    ZipArchive tmpArchive = new ZipArchive(new DiskFile("c:/SuperSport/Motorsport/Xml/History/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.ToString("yyyyMMdd") + ".zip"));
                    tmpFolder.MoveFilesTo(tmpArchive, true, true);
                }
                catch (Exception ex)
                {
                    _logger.Error("Error occurred in archiveFiles()", ex);

                    curRun.Errors.Add("Zip - " + ex.Message);
                }
            }
        }

        private void checkFolders()
        {
            if (!Directory.Exists("c:/SuperSport"))
            {
                Directory.CreateDirectory("c:/SuperSport");
            }
            if (!Directory.Exists("c:/SuperSport/Motorsport"))
            {
                Directory.CreateDirectory("c:/SuperSport/Motorsport");
            }
            if (!Directory.Exists("c:/SuperSport/Motorsport/Xml"))
            {
                Directory.CreateDirectory("c:/SuperSport/Motorsport/Xml");
            }
            if (!Directory.Exists("c:/SuperSport/Motorsport/Xml/Temp"))
            {
                Directory.CreateDirectory("c:/SuperSport/Motorsport/Xml/Temp");
            }
            if (!Directory.Exists("c:/SuperSport/Motorsport/Xml/Processed"))
            {
                Directory.CreateDirectory("c:/SuperSport/Motorsport/Xml/Processed");
            }
            if (!Directory.Exists("c:/SuperSport/Motorsport/Xml/Unprocessed"))
            {
                Directory.CreateDirectory("c:/SuperSport/Motorsport/Xml/Unprocessed");
            }
            if (!Directory.Exists("c:/SuperSport/Motorsport/Xml/History"))
            {
                Directory.CreateDirectory("c:/SuperSport/Motorsport/Xml/History");
            }
            if (!Directory.Exists("c:/SuperSport/Motorsport/Xml/History/" + DateTime.Now.Year.ToString()))
            {
                Directory.CreateDirectory("c:/SuperSport/Motorsport/Xml/History/" + DateTime.Now.Year.ToString());
            }
        }
    }

    public class runInfo
    {
        public DateTime Start;
        public DateTime End;
        public int Downloads;
        public int DownloadErrors;
        public List<string> Errors = new List<string>();
    }

    public class Session
    {
        public int RaceId { get; set; }
        public int RaceSport { get; set; }
        public int RaceCompetition { get; set; }
        public string RaceName { get; set; }
        public string RaceVenue { get; set; }
        public int Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public int Laps { get; set; }
        public string Minutes { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
        public DateTime FileTime { get; set; }
    }

    public class Leaderboard
    {
        public int Session { get; set; }
        public int Position { get; set; }
        public string PositionText { get; set; }
        public int DriverId { get; set; }
        public string DriverName { get; set; }
        public string DriverCountry { get; set; }
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public string Gap { get; set; }
        public int Laps { get; set; }
        public string Time { get; set; }
        public int Pits { get; set; }
        public string Comment { get; set; }
    }
}
